puts "Create Admin User"
user = AdminUser.new(email: 'admin@mycompliance.com', password: 'password', password_confirmation: 'password', name: 'admin')
user.skip_confirmation!
user.save!
user.add_role :super_admin
puts "Done"

puts "Create User"
user = User.new(email: 'test@mycompliance.com', first_name: "test", last_name: "test", user_name: "test_mc", password: 'password', password_confirmation: 'password')
user.skip_confirmation!
user.save!
user.add_role :company
puts "Done"