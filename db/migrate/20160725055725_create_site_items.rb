class CreateSiteItems < ActiveRecord::Migration
  def change
    create_table :site_items do |t|
      t.integer :user_id
      t.integer :site_id
      t.integer :site_tab_id
      t.string :title
      t.integer :frequency
      t.boolean :required_for_site_compliance, default: false
      t.datetime :due_date
      t.string :due_soon

      t.timestamps null: false
    end
  end
end
