class AddDeletedAtToTables < ActiveRecord::Migration
  def change
    add_column :users, :deleted_at, :datetime
    add_index :users, :deleted_at
    
    add_column :sites, :deleted_at, :datetime
    add_index :sites, :deleted_at
    
    add_column :site_items, :deleted_at, :datetime
    add_index :site_items, :deleted_at
    
    add_column :static_pages, :deleted_at, :datetime
    add_index :static_pages, :deleted_at
  end
end
