class CreateSiteSettings < ActiveRecord::Migration
  def change
    create_table :site_settings do |t|
      t.integer :maximum_site_tabs
      t.integer :maximum_site_items

      t.timestamps null: false
    end
  end
end
