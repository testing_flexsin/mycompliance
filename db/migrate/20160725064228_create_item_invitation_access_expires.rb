class CreateItemInvitationAccessExpires < ActiveRecord::Migration
  def change
    create_table :item_invitation_access_expires do |t|
      t.string :expires

      t.timestamps null: false
    end
  end
end
