class RemoveDocumentInvitationFromEmailTemplates < ActiveRecord::Migration
  def change
    remove_column :email_templates, :document_invitation, :text
  end
end
