class CreateStaticPages < ActiveRecord::Migration
  def change
    create_table :static_pages do |t|
      t.string :logo
      t.text :header_logged_in
      t.text :header_not_logged_in
      t.text :footer
      t.text :features
      t.text :about_us
      t.text :terms_and_conditions
      t.text :privacy_policy
      t.text :faq
      t.text :contact_us
      t.text :services
      t.text :pricing
      t.text :news
      t.text :team
      t.text :testimonials

      t.timestamps null: false
    end
  end
end
