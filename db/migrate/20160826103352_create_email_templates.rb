class CreateEmailTemplates < ActiveRecord::Migration
  def change
    create_table :email_templates do |t|
    	t.text :admin_email
      t.text :user_confirmation_instruction
      t.text :user_invitation_instruction
      t.text :user_password_change
      t.text :user_password_update
      t.text :user_reset_password_instruction
      t.text :user_inquiry
      t.text :user_newsletter
      t.text :user_contact_us
      t.text :admin_confirmation_instruction
      t.text :admin_password_updated
      t.text :admin_account_created
      t.text :admin_reset_password_instruction
      t.text :admin_inquiry
      t.text :admin_newsletter
      t.text :admin_contact_us

      t.timestamps null: false
    end
  end
end
