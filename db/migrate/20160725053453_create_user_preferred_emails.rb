class CreateUserPreferredEmails < ActiveRecord::Migration
  def change
    create_table :user_preferred_emails do |t|
      t.integer :user_id
      t.string :first_name
      t.string :last_name
      t.string :full_name
      t.string :email
      t.boolean :pause, default: false

      t.timestamps null: false
    end
  end
end
