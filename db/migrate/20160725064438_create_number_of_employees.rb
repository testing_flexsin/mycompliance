class CreateNumberOfEmployees < ActiveRecord::Migration
  def change
    create_table :number_of_employees do |t|
      t.string :employees

      t.timestamps null: false
    end
  end
end
