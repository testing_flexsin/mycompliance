class CreateCompanyInformations < ActiveRecord::Migration
  def change
    create_table :company_informations do |t|
      t.integer :user_id
      t.string :name
      t.string :city
      t.string :state
      t.string :country
      t.string :zip_code
      t.string :organization_type
      t.string :number_of_employees
      t.string :logo

      t.timestamps null: false
    end
  end
end
