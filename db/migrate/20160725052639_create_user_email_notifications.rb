class CreateUserEmailNotifications < ActiveRecord::Migration
  def change
    create_table :user_email_notifications do |t|
      t.integer :user_id
      t.string :due_soon
      t.string :due_soon_often
      t.string :overdue
      t.string :overdue_often
      t.string :due_soon_invitee
      t.string :due_soon_often_invitee
      t.string :overdue_invitee
      t.string :overdue_often_invitee

      t.timestamps null: false
    end
  end
end
