class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.integer :user_id
      t.integer :site_id
      t.string :sites
      t.string :report_type
      t.string :tabs, array: true, default: []
      t.string :items, array: true, default: []
      t.string :documents, array: true, default: []
      t.integer :document_expires, default: 0
      t.string :report_format, array: true, default: []
      t.boolean :lock_document, default: false
      t.string :send_report, array: true, default: []
      t.string :to_email, array: true, default: []
      t.string :cc_email, array: true, default: []

      t.timestamps null: false
    end
  end
end
