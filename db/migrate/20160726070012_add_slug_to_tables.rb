class AddSlugToTables < ActiveRecord::Migration
  def change
    add_column :users, :slug, :string
    add_index :users, :slug, unique: true
    
    add_column :sites, :slug, :string
    add_index :sites, :slug, unique: true
    
    add_column :site_tabs, :slug, :string
    add_index :site_tabs, :slug, unique: true
    
    add_column :site_items, :slug, :string
    add_index :site_items, :slug, unique: true
    
    add_column :news, :slug, :string
    add_index :news, :slug, unique: true
    
    add_column :teams, :slug, :string
    add_index :teams, :slug, unique: true
  end
end
