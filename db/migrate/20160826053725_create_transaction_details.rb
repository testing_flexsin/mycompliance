class CreateTransactionDetails < ActiveRecord::Migration
  def change
    create_table :transaction_details do |t|
			t.integer :user_id
			t.integer :plan_id
			t.string :payer_id
			t.string :payer_email
      t.datetime :payment_date
      t.string :payment_status
      t.float :payment_fee
      t.float :payment
      t.string :payment_type
			t.string :mc_currency
      t.string :first_name
      t.string :last_name
      t.string :txn_id
      t.string :receiver_id
      t.string :receiver_email
      t.string :address_name
      t.string :address_street
      t.string :address_city
      t.string :address_state
      t.string :address_country
      t.string :address_country_code
      t.string :address_zip

      t.timestamps null: false
    end
  end
end
