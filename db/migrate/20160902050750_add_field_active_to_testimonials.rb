class AddFieldActiveToTestimonials < ActiveRecord::Migration
  def change
    add_column :testimonials, :active, :boolean, default: true
  end
end
