class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.integer :news_id
      t.string :user_name
      t.string :email
      t.text :message

      t.timestamps null: false
    end
  end
end
