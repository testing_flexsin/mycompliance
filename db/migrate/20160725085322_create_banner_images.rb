class CreateBannerImages < ActiveRecord::Migration
  def change
    create_table :banner_images do |t|
      t.string :image
      t.text :description
      t.boolean :active, default: true

      t.timestamps null: false
    end
  end
end
