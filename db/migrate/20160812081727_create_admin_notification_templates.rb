class CreateAdminNotificationTemplates < ActiveRecord::Migration
  def change
    create_table :admin_notification_templates do |t|
      t.text :invitee_template
      t.text :email_due_soon_template
      t.text :email_overdue_template
      t.text :email_due_soon_invitee_template
      t.text :email_overdue_invitee_template
      t.text :email_report_recipients_template
      t.text :document_feedback_approved_template
      t.text :document_feedback_pending_template
      t.text :document_feedback_rejected_template

      t.timestamps null: false
    end
  end
end
