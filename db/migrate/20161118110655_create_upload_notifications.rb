class CreateUploadNotifications < ActiveRecord::Migration
  def change
    create_table :upload_notifications do |t|
      t.integer :user_id
      t.integer :site_item_document_id
      t.text :body
      t.boolean :is_read, default: false
      t.datetime :read_at

      t.timestamps null: false
    end
  end
end
