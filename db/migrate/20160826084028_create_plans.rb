class CreatePlans < ActiveRecord::Migration
  def change
    create_table :plans do |t|
      t.string :name
      t.float :price
      t.integer :time_duration
      t.string :time_duration_postfix

      t.timestamps null: false
    end
  end
end
