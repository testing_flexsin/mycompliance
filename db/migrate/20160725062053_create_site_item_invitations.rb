class CreateSiteItemInvitations < ActiveRecord::Migration
  def change
    create_table :site_item_invitations do |t|
      t.integer :site_id
      t.integer :site_item_id
      t.integer :user_id
      t.string :access_expires
      t.datetime :access_expires_date
      t.string :upload_access_email
      t.text :comment
      t.boolean :pause, default: false

      t.timestamps null: false
    end
  end
end
