class AddColumnPermissionsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :permissions, :string
  end
end
