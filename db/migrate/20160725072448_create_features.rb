class CreateFeatures < ActiveRecord::Migration
  def change
    create_table :features do |t|
      t.string :image
      t.text :description

      t.timestamps null: false
    end
  end
end
