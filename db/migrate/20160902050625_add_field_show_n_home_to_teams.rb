class AddFieldShowNHomeToTeams < ActiveRecord::Migration
  def change
    add_column :teams, :show_on_home, :boolean, default: false
  end
end
