class CreateSiteItemDocuments < ActiveRecord::Migration
  def change
    create_table :site_item_documents do |t|
      t.integer :site_id
      t.integer :site_item_id
      t.integer :user_id
      t.string :file
      t.boolean :archive, default: false
      t.boolean :automatic_archive, default: false
      t.boolean :current, default: false
      t.boolean :pending, default: false
      t.boolean :approve, default: false
      t.text :feedback
      t.string :contractor_email

      t.timestamps null: false
    end
  end
end
