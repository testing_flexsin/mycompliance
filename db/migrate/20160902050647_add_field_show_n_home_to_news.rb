class AddFieldShowNHomeToNews < ActiveRecord::Migration
  def change
    add_column :news, :show_on_home, :boolean, default: false
  end
end
