class CreateSiteItemQuestions < ActiveRecord::Migration
  def change
    create_table :site_item_questions do |t|
      t.integer :site_id
      t.integer :site_item_id
      t.string :question
      t.boolean :required_for_compliant, default: true

      t.timestamps null: false
    end
  end
end
