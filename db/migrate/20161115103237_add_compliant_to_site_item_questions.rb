class AddCompliantToSiteItemQuestions < ActiveRecord::Migration
  def change
    add_column :site_item_questions, :compliant, :boolean, default: false
  end
end
