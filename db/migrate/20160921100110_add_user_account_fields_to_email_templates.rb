class AddUserAccountFieldsToEmailTemplates < ActiveRecord::Migration
  def change
    add_column :email_templates, :account_created, :text
    add_column :email_templates, :password_updated, :text
  end
end
