class AddFieldSocialLinksToTeams < ActiveRecord::Migration
  def change
    add_column :teams, :facebook, :string
    add_column :teams, :twitter, :string
    add_column :teams, :linkedin, :string
    add_column :teams, :gmail, :string
  end
end
