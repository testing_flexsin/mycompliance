class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :image
      t.string :name
      t.text :description
      t.string :slug

      t.timestamps null: false
    end
  end
end
