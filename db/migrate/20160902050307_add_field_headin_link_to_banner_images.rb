class AddFieldHeadinLinkToBannerImages < ActiveRecord::Migration
  def change
    add_column :banner_images, :heading, :string
    add_column :banner_images, :link, :string
  end
end
