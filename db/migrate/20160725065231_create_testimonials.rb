class CreateTestimonials < ActiveRecord::Migration
  def change
    create_table :testimonials do |t|
      t.string :name
      t.string :image
      t.text :message
      t.string :location

      t.timestamps null: false
    end
  end
end
