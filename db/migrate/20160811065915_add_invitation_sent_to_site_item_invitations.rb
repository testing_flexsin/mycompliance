class AddInvitationSentToSiteItemInvitations < ActiveRecord::Migration
  def change
    add_column :site_item_invitations, :invitation_sent, :boolean, default: false
  end
end
