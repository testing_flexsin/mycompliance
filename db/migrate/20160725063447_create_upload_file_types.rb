class CreateUploadFileTypes < ActiveRecord::Migration
  def change
    create_table :upload_file_types do |t|
      t.string :upload_type

      t.timestamps null: false
    end
  end
end
