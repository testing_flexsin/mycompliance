class AddStatusFieldsToSites < ActiveRecord::Migration
  def change
    add_column :sites, :status, :string
    add_column :sites, :compliant, :boolean, default: false
    add_column :sites, :active, :boolean, default: true
  end
end
