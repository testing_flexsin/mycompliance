class AddJobPhoneActiveToAdminUsers < ActiveRecord::Migration
  def change
    add_column :admin_users, :name, :string
    add_column :admin_users, :job_title, :string
    add_column :admin_users, :phone_number, :string
    add_column :admin_users, :active, :boolean, default: true
    add_column :admin_users, :slug, :string
  end
end
