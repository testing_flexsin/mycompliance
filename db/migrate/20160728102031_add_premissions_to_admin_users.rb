class AddPremissionsToAdminUsers < ActiveRecord::Migration
  def change
    add_column :admin_users, :permissions, :string
  end
end
