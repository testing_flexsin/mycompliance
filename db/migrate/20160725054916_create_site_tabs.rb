class CreateSiteTabs < ActiveRecord::Migration
  def change
    create_table :site_tabs do |t|
      t.integer :user_id
      t.integer :site_id
      t.string :name
      t.integer :items

      t.timestamps null: false
    end
  end
end
