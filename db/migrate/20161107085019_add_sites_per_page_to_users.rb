class AddSitesPerPageToUsers < ActiveRecord::Migration
  def change
    add_column :users, :sites_per_page, :integer, default: 10
  end
end
