class CreateSites < ActiveRecord::Migration
  def change
    create_table :sites do |t|
      t.integer :user_id
      t.string :name
      t.datetime :annual_compliance_due
      t.integer :tabs
      t.integer :tab_items
      t.integer :site_item_id

      t.timestamps null: false
    end
  end
end
