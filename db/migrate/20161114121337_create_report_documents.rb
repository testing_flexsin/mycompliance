class CreateReportDocuments < ActiveRecord::Migration
  def change
    create_table :report_documents do |t|
      t.integer :report_id
      t.string :document

      t.timestamps null: false
    end
  end
end
