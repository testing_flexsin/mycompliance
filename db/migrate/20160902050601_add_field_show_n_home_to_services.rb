class AddFieldShowNHomeToServices < ActiveRecord::Migration
  def change
    add_column :services, :show_on_home, :boolean, default: false
  end
end
