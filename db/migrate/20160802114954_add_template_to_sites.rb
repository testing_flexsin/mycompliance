class AddTemplateToSites < ActiveRecord::Migration
  def change
    add_column :sites, :template, :boolean, default: false
  end
end
