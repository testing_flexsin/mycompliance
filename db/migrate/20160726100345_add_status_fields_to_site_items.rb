class AddStatusFieldsToSiteItems < ActiveRecord::Migration
  def change
    add_column :site_items, :status, :string
    add_column :site_items, :compliant, :boolean, default: false
    add_column :site_items, :template, :boolean, default: false
  end
end
