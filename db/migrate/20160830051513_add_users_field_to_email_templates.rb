class AddUsersFieldToEmailTemplates < ActiveRecord::Migration
  def change
    add_column :email_templates, :manager_account, :text
    add_column :email_templates, :manager_created, :text
    add_column :email_templates, :manager_updated, :text
    add_column :email_templates, :document_invitation, :text
    add_column :email_templates, :invitation_confirmation, :text
    change_column :email_templates, :admin_email, :string 
  end
end
