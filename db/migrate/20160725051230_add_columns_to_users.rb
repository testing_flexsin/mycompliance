class AddColumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :full_name, :string
    add_column :users, :user_name, :string
    add_column :users, :mobile_number, :string
    add_column :users, :landline_number, :string
    add_column :users, :time_zone, :string
    add_column :users, :job_title, :string
    add_column :users, :employee_id, :string
    add_column :users, :avatar, :string
    add_column :users, :active, :boolean, default: true
  end
end
