# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161118110655) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_notification_templates", force: :cascade do |t|
    t.text     "invitee_template"
    t.text     "email_due_soon_template"
    t.text     "email_overdue_template"
    t.text     "email_due_soon_invitee_template"
    t.text     "email_overdue_invitee_template"
    t.text     "email_report_recipients_template"
    t.text     "document_feedback_approved_template"
    t.text     "document_feedback_pending_template"
    t.text     "document_feedback_rejected_template"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "",   null: false
    t.string   "encrypted_password",     default: "",   null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "name"
    t.string   "job_title"
    t.string   "phone_number"
    t.boolean  "active",                 default: true
    t.string   "slug"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "permissions"
  end

  add_index "admin_users", ["confirmation_token"], name: "index_admin_users_on_confirmation_token", unique: true, using: :btree
  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "admin_users_roles", id: false, force: :cascade do |t|
    t.integer "admin_user_id"
    t.integer "role_id"
  end

  add_index "admin_users_roles", ["admin_user_id", "role_id"], name: "index_admin_users_roles_on_admin_user_id_and_role_id", using: :btree

  create_table "banner_images", force: :cascade do |t|
    t.string   "image"
    t.text     "description"
    t.boolean  "active",      default: true
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "heading"
    t.string   "link"
  end

  create_table "comment_hierarchies", id: false, force: :cascade do |t|
    t.integer "ancestor_id",   null: false
    t.integer "descendant_id", null: false
    t.integer "generations",   null: false
  end

  add_index "comment_hierarchies", ["ancestor_id", "descendant_id", "generations"], name: "comment_anc_desc_idx", unique: true, using: :btree
  add_index "comment_hierarchies", ["descendant_id"], name: "comment_desc_idx", using: :btree

  create_table "comments", force: :cascade do |t|
    t.integer  "news_id"
    t.string   "user_name"
    t.string   "email"
    t.text     "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "parent_id"
  end

  create_table "company_informations", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.string   "zip_code"
    t.string   "organization_type"
    t.string   "number_of_employees"
    t.string   "logo"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "contact_us", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "contact_number"
    t.text     "message"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "email_templates", force: :cascade do |t|
    t.string   "admin_email"
    t.text     "user_confirmation_instruction"
    t.text     "user_invitation_instruction"
    t.text     "user_password_change"
    t.text     "user_password_update"
    t.text     "user_reset_password_instruction"
    t.text     "user_inquiry"
    t.text     "user_newsletter"
    t.text     "user_contact_us"
    t.text     "admin_confirmation_instruction"
    t.text     "admin_password_updated"
    t.text     "admin_account_created"
    t.text     "admin_reset_password_instruction"
    t.text     "admin_inquiry"
    t.text     "admin_newsletter"
    t.text     "admin_contact_us"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.text     "manager_account"
    t.text     "manager_created"
    t.text     "manager_updated"
    t.text     "invitation_confirmation"
    t.text     "account_created"
    t.text     "password_updated"
  end

  create_table "features", force: :cascade do |t|
    t.string   "image"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "inquiries", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "contact_number"
    t.text     "message"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "item_invitation_access_expires", force: :cascade do |t|
    t.string   "expires"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "news", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.string   "image"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "slug"
    t.boolean  "show_on_home", default: false
  end

  add_index "news", ["slug"], name: "index_news_on_slug", unique: true, using: :btree

  create_table "newsletters", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "number_of_employees", force: :cascade do |t|
    t.string   "employees"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "organization_types", force: :cascade do |t|
    t.string   "org_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "plans", force: :cascade do |t|
    t.string   "name"
    t.float    "price"
    t.integer  "time_duration"
    t.string   "time_duration_postfix"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "report_documents", force: :cascade do |t|
    t.integer  "report_id"
    t.string   "document"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "reports", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "site_id"
    t.string   "sites"
    t.string   "report_type"
    t.string   "tabs",             default: [],                 array: true
    t.string   "items",            default: [],                 array: true
    t.string   "documents",        default: [],                 array: true
    t.integer  "document_expires", default: 0
    t.string   "report_format",    default: [],                 array: true
    t.boolean  "lock_document",    default: false
    t.string   "send_report",      default: [],                 array: true
    t.string   "to_email",         default: [],                 array: true
    t.string   "cc_email",         default: [],                 array: true
    t.json     "reports"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "services", force: :cascade do |t|
    t.string   "image"
    t.string   "name"
    t.text     "description"
    t.string   "slug"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.boolean  "show_on_home", default: false
  end

  create_table "site_item_documents", force: :cascade do |t|
    t.integer  "site_id"
    t.integer  "site_item_id"
    t.integer  "user_id"
    t.string   "file"
    t.boolean  "archive",           default: false
    t.boolean  "automatic_archive", default: false
    t.boolean  "current",           default: false
    t.boolean  "pending",           default: false
    t.boolean  "approve",           default: false
    t.text     "feedback"
    t.string   "contractor_email"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "site_item_invitations", force: :cascade do |t|
    t.integer  "site_id"
    t.integer  "site_item_id"
    t.integer  "user_id"
    t.string   "access_expires"
    t.datetime "access_expires_date"
    t.string   "upload_access_email"
    t.text     "comment"
    t.boolean  "pause",               default: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.boolean  "invitation_sent",     default: false
  end

  create_table "site_item_questions", force: :cascade do |t|
    t.integer  "site_id"
    t.integer  "site_item_id"
    t.string   "question"
    t.boolean  "required_for_compliant", default: true
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "compliant",              default: false
  end

  create_table "site_items", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "site_id"
    t.integer  "site_tab_id"
    t.string   "title"
    t.integer  "frequency"
    t.boolean  "required_for_site_compliance", default: false
    t.datetime "due_date"
    t.string   "due_soon"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.string   "slug"
    t.datetime "deleted_at"
    t.string   "status"
    t.boolean  "compliant",                    default: false
    t.boolean  "template",                     default: false
  end

  add_index "site_items", ["deleted_at"], name: "index_site_items_on_deleted_at", using: :btree
  add_index "site_items", ["slug"], name: "index_site_items_on_slug", unique: true, using: :btree

  create_table "site_settings", force: :cascade do |t|
    t.integer  "maximum_site_tabs"
    t.integer  "maximum_site_items"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "site_tabs", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "site_id"
    t.string   "name"
    t.integer  "items"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "slug"
  end

  add_index "site_tabs", ["slug"], name: "index_site_tabs_on_slug", unique: true, using: :btree

  create_table "sites", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name"
    t.datetime "annual_compliance_due"
    t.integer  "tabs"
    t.integer  "tab_items"
    t.integer  "site_item_id"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "slug"
    t.datetime "deleted_at"
    t.string   "status"
    t.boolean  "compliant",             default: false
    t.boolean  "active",                default: true
    t.boolean  "template",              default: false
  end

  add_index "sites", ["deleted_at"], name: "index_sites_on_deleted_at", using: :btree
  add_index "sites", ["slug"], name: "index_sites_on_slug", unique: true, using: :btree

  create_table "static_pages", force: :cascade do |t|
    t.string   "logo"
    t.text     "header_logged_in"
    t.text     "header_not_logged_in"
    t.text     "footer"
    t.text     "features"
    t.text     "about_us"
    t.text     "terms_and_conditions"
    t.text     "privacy_policy"
    t.text     "faq"
    t.text     "contact_us"
    t.text     "services"
    t.text     "pricing"
    t.text     "news"
    t.text     "team"
    t.text     "testimonials"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.datetime "deleted_at"
  end

  add_index "static_pages", ["deleted_at"], name: "index_static_pages_on_deleted_at", using: :btree

  create_table "teams", force: :cascade do |t|
    t.string   "name"
    t.string   "image"
    t.text     "description"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "slug"
    t.boolean  "show_on_home", default: false
    t.string   "facebook"
    t.string   "twitter"
    t.string   "linkedin"
    t.string   "gmail"
  end

  add_index "teams", ["slug"], name: "index_teams_on_slug", unique: true, using: :btree

  create_table "testimonials", force: :cascade do |t|
    t.string   "name"
    t.string   "image"
    t.text     "message"
    t.string   "location"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.boolean  "active",     default: true
  end

  create_table "transaction_details", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "plan_id"
    t.string   "payer_id"
    t.string   "payer_email"
    t.datetime "payment_date"
    t.string   "payment_status"
    t.float    "payment_fee"
    t.float    "payment"
    t.string   "payment_type"
    t.string   "mc_currency"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "txn_id"
    t.string   "receiver_id"
    t.string   "receiver_email"
    t.string   "address_name"
    t.string   "address_street"
    t.string   "address_city"
    t.string   "address_state"
    t.string   "address_country"
    t.string   "address_country_code"
    t.string   "address_zip"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "upload_file_types", force: :cascade do |t|
    t.string   "upload_type"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "upload_notifications", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "site_item_document_id"
    t.text     "body"
    t.boolean  "is_read",               default: false
    t.datetime "read_at"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  create_table "user_email_notifications", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "due_soon"
    t.string   "due_soon_often"
    t.string   "overdue"
    t.string   "overdue_often"
    t.string   "due_soon_invitee"
    t.string   "due_soon_often_invitee"
    t.string   "overdue_invitee"
    t.string   "overdue_often_invitee"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "user_notification_templates", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "invitee_template"
    t.text     "email_due_soon_template"
    t.text     "email_overdue_template"
    t.text     "email_due_soon_invitee_template"
    t.text     "email_overdue_invitee_template"
    t.text     "email_report_recipients_template"
    t.text     "document_feedback_approved_template"
    t.text     "document_feedback_pending_template"
    t.text     "document_feedback_rejected_template"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "user_preferred_emails", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "full_name"
    t.string   "email"
    t.boolean  "pause",      default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: ""
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
    t.integer  "invitations_count",      default: 0
    t.string   "first_name"
    t.string   "last_name"
    t.string   "full_name"
    t.string   "user_name"
    t.string   "mobile_number"
    t.string   "landline_number"
    t.string   "time_zone"
    t.string   "job_title"
    t.string   "employee_id"
    t.string   "avatar"
    t.boolean  "active",                 default: true
    t.string   "slug"
    t.datetime "deleted_at"
    t.integer  "company_id"
    t.integer  "plan_id"
    t.string   "permissions"
    t.boolean  "admin",                  default: false
    t.integer  "sites_per_page",         default: 10
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["deleted_at"], name: "index_users_on_deleted_at", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["invitation_token"], name: "index_users_on_invitation_token", unique: true, using: :btree
  add_index "users", ["invitations_count"], name: "index_users_on_invitations_count", using: :btree
  add_index "users", ["invited_by_id"], name: "index_users_on_invited_by_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["slug"], name: "index_users_on_slug", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

  create_table "version_associations", force: :cascade do |t|
    t.integer "version_id"
    t.string  "foreign_key_name", null: false
    t.integer "foreign_key_id"
  end

  add_index "version_associations", ["foreign_key_name", "foreign_key_id"], name: "index_version_associations_on_foreign_key", using: :btree
  add_index "version_associations", ["version_id"], name: "index_version_associations_on_version_id", using: :btree

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",      null: false
    t.integer  "item_id",        null: false
    t.string   "event",          null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
    t.integer  "transaction_id"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree
  add_index "versions", ["transaction_id"], name: "index_versions_on_transaction_id", using: :btree

end
