ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    columns do
      column do
        panel "Site DBs" do
          ul(class: "list-none") do
            li link_to("Sites", admin_sites_path)
            li link_to("Site Tabs", admin_site_tabs_path)
            li link_to("Site Items", admin_site_items_path)
            li link_to("Site Item Questions", admin_site_item_questions_path)
            li link_to("Site Item Documents", admin_site_item_documents_path)
            li link_to("Site Item Invitations", admin_site_item_invitations_path)
            li link_to("Upload site", admin_upload_site_path)
          end
        end
        panel "User DBs" do
          ul(class: "list-none") do
            li link_to("Admin Users", admin_admin_users_path)
            li link_to("Users", admin_users_path)
            li link_to("Company Informations", admin_company_informations_path)
            li link_to("User Email Notifications", admin_user_email_notifications_path)
            li link_to("User Notification Templates", admin_user_notification_templates_path)
            li link_to("Admin Notification Templates", admin_admin_notification_templates_path)
            li link_to("User Preferred Emails", admin_user_preferred_emails_path)
          end
        end
        panel "DBs" do
          ul(class: "list-none") do
            li link_to("Static Pages", admin_static_pages_path)
            li link_to("Contact Us", admin_contact_us_path)
            li link_to("Banner Images", admin_banner_images_path)
            li link_to("Features", admin_features_path)
            li link_to("Inquiries", admin_inquiries_path)
            li link_to("Item Invitation Access Expires", admin_item_invitation_access_expires_path)
            li link_to("News", admin_news_index_path)
            li link_to("News Comments", admin_news_comments_path)
            li link_to("Newsletters", admin_newsletters_path)
            li link_to("Number Of Employees", admin_number_of_employees_path)
            li link_to("Organization Types", admin_organization_types_path)
            li link_to("Site Settings", admin_site_settings_path)
            li link_to("Teams", admin_teams_path)
            li link_to("Services", admin_services_path)
            li link_to("Testimonials", admin_testimonials_path)
            li link_to("Upload File Types", admin_upload_file_types_path)
            li link_to("Comments", admin_comments_path)
            li link_to("Transaction Details", admin_transaction_details_path)
            li link_to("Plans", admin_plans_path)
            li link_to("Email Template", admin_email_templates_path)
          end
        end
      end
    end
    # div class: "blank_slate_container", id: "dashboard_default_message" do
    #   span class: "blank_slate" do
    #     span I18n.t("active_admin.dashboard_welcome.welcome")
    #     small I18n.t("active_admin.dashboard_welcome.call_to_action")
    #   end
    # end
  end # content
end
