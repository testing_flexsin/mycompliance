ActiveAdmin.register Plan do
	menu false

	permit_params :name, :price, :time_duration, :time_duration_postfix
end
