ActiveAdmin.register News do
	menu false
	
	permit_params :title, :image, :description, :show_on_home
	
	filter :title
	filter :description
	filter :show_on_home
	filter :created_at, as: :date_range
	filter :updated_at, as: :date_range

	index do 
		selectable_column
		id_column
		column :title
		column "Image" do |news|
			image_tag news.image, class: "image-preview-small" if news.image.present? && news.image.file.exists?
		end
		column :description
		column :show_on_home
		column :created_at
		column :updated_at
		actions
	end

	show do
		attributes_table do
			row :id
			row :title
			row "Image" do |news|
				image_tag news.image, class: "image-preview-small" if news.image.present? && news.image.file.exists?
			end
			row :description
			row :show_on_home
			row :created_at
			row :updated_at
		end
		active_admin_comments
	end

	form do |f|
		f.inputs do
	    f.input :title
			f.input :image, as: :file, image_preview: true
			f.input :description
			f.input :show_on_home
		end
    f.actions
  end

	csv do
		column :id
		column :title
		column "Image" do |news|
			if news.image.present? && news.image.file.exists?
	      a = '=HYPERLINK("link", "image_identifier")'
	      a = a.gsub("link", ENV["DOMAIN"] + news.image.url).gsub("image_identifier", news.image_identifier) rescue nil
	    end
	  end
		column :description
		column :show_on_home
		column :created_at
		column :updated_at
  end
	
	controller do
		def find_resource
		  begin
		    scoped_collection.friendly.find(params[:id])
		  rescue ActiveRecord::RecordNotFound
		    scoped_collection.find(params[:id])
		  end
		end
	end
end
