ActiveAdmin.register AdminNotificationTemplate do
	menu false

	permit_params :invitee_template, :email_due_soon_template, :email_overdue_template, :email_due_soon_invitee_template, :email_overdue_invitee_template, :email_report_recipients_template, :document_feedback_approved_template, :document_feedback_pending_template, :document_feedback_rejected_template

	filter :invitee_template
	filter :email_due_soon_template
	filter :email_overdue_template
	filter :email_due_soon_invitee_template
	filter :email_overdue_invitee_template
	filter :email_report_recipients_template
	filter :document_feedback_approved_template
	filter :document_feedback_pending_template
	filter :document_feedback_rejected_template
	filter :created_at, as: :date_range
	filter :updated_at, as: :date_range

	index do 
		selectable_column
		id_column
		column :invitee_template
		column :email_due_soon_template
		column :email_overdue_template
		column :email_due_soon_invitee_template
		column :email_overdue_invitee_template
		column :email_report_recipients_template
		column :document_feedback_approved_template
		column :document_feedback_pending_template
		column :document_feedback_rejected_template
		column :created_at
		column :updated_at
		actions
	end

	show do
		attributes_table do
			row :id
			row :invitee_template
			row :email_due_soon_template
			row :email_overdue_template
			row :email_due_soon_invitee_template
			row :email_overdue_invitee_template
			row :email_report_recipients_template
			row :document_feedback_approved_template
			row :document_feedback_pending_template
			row :document_feedback_rejected_template
			row :created_at
			row :updated_at
		end
		active_admin_comments
	end

	form do |f|
		f.inputs do
			f.input :invitee_template
			f.input :email_due_soon_template
			f.input :email_overdue_template
			f.input :email_due_soon_invitee_template
			f.input :email_overdue_invitee_template
			f.input :email_report_recipients_template
			f.input :document_feedback_approved_template
			f.input :document_feedback_pending_template
			f.input :document_feedback_rejected_template
		end
    f.actions
  end

  csv do
		column :id
    column :invitee_template
		column :email_due_soon_template
		column :email_overdue_template
		column :email_due_soon_invitee_template
		column :email_overdue_invitee_template
		column :email_report_recipients_template
		column :document_feedback_approved_template
		column :document_feedback_pending_template
		column :document_feedback_rejected_template
		column :created_at
		column :updated_at
  end
end
