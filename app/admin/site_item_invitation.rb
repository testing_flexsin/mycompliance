ActiveAdmin.register SiteItemInvitation do
	menu false

	# permit_params :site_id, :site_item_id, :user_id, :access_expires, :access_expires_date, :upload_access_email, :comment, :pause

	filter :user, as: :select, collection: ->{User.with_any_role(:company, :manager).collect {|user| [user.email, user.id]}}
	filter :site
	filter :access_expires	
	filter :access_expires_date
	filter :upload_access_email
	filter :comment
	filter :pause
	filter :created_at, as: :date_range
	filter :updated_at, as: :date_range

	index do
		selectable_column
		id_column
		column "User Id" do |site_item_invitation|
			link_to "User ##{site_item_invitation.user_id}", admin_user_path(site_item_invitation.user_id) if site_item_invitation.user_id.present?
		end
		column "Site Id" do |site_item_invitation|
			link_to "Site ##{site_item_invitation.site_id}", admin_site_path(site_item_invitation.site_id) if site_item_invitation.site_id.present?
		end 
		column "Site Item Id" do |site_item_invitation|
			link_to "Site Item ##{site_item_invitation.site_item_id}", admin_site_item_path(site_item_invitation.site_item_id) if site_item_invitation.site_item_id.present?
		end
		column :access_expires	
		column :access_expires_date
		column :upload_access_email
		column :comment
		column :pause
		column :created_at
		column :updated_at
    actions
	end
	
	show do
		attributes_table do
			row :id
			row "User Id" do |site_item_invitation|
				link_to "User ##{site_item_invitation.user_id}", admin_user_path(site_item_invitation.user_id) if site_item_invitation.user_id.present?
			end
			row "Site Id" do |site_item_invitation|
				link_to "Site ##{site_item_invitation.site_id}", admin_site_path(site_item_invitation.site_id) if site_item_invitation.site_id.present?
			end
			row "Site Item Id" do |site_item_invitation|
				link_to "Site Item ##{site_item_invitation.site_item_id}", admin_site_item_path(site_item_invitation.site_item_id) if site_item_invitation.site_item_id.present?
			end
			row :access_expires	
			row :access_expires_date
			row :upload_access_email
			row :comment
			row :pause
			row :created_at
			row :updated_at
		end
		active_admin_comments
	end

	csv do
		column :id
		column "User Id" do |site_item_invitation|
			if site_item_invitation.user_id.present?
	      a = '=HYPERLINK("link", "user_id")'
	      a = a.gsub("link", admin_user_url(site_item_invitation.user_id)).gsub("user_id", "User ##{site_item_invitation.user_id}") rescue nil
	    end
		end
		column "Site Id" do |site_item_invitation|
			if site_item_invitation.site_id.present?
	      a = '=HYPERLINK("link", "site_id")'
	      a = a.gsub("link", admin_site_url(site_item_invitation.site_id)).gsub("site_id", "Site ##{site_item_invitation.site_id}") rescue nil
	    end
		end 
		column "Site Item Id" do |site_item_invitation|
			if site_item_invitation.site_item_id.present?
	      a = '=HYPERLINK("link", "site_item_id")'
	      a = a.gsub("link", admin_site_item_url(site_item_invitation.site_item_id)).gsub("site_item_id", "Site ##{site_item_invitation.site_item_id}") rescue nil
	    end
		end
		column :access_expires	
		column :access_expires_date
		column :upload_access_email
		column :comment
		column :pause
		column :created_at
		column :updated_at
  end
	
	actions :index, :show
end
