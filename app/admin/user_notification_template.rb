ActiveAdmin.register UserNotificationTemplate do
	menu false
	
	permit_params :id, :user_id, :invitee_template, :email_due_soon_template, :email_overdue_template, :email_due_soon_invitee_template, :email_overdue_invitee_template, :email_report_recipients_template, :document_feedback_approved_template, :document_feedback_pending_template, :document_feedback_rejected_template

	filter :user, as: :select, collection: ->{User.order("id ASC").collect {|user| [user.email, user.id]}}
	filter :invitee_template
	filter :email_due_soon_template
	filter :email_overdue_template
	filter :email_due_soon_invitee_template
	filter :email_overdue_invitee_template
	filter :email_report_recipients_template
	filter :document_feedback_approved_template
	filter :document_feedback_pending_template
	filter :document_feedback_rejected_template
	filter :created_at, as: :date_range
	filter :updated_at, as: :date_range

	index do 
		selectable_column
			id_column
			column "User Id" do |user_notification_template|
				link_to "User ##{user_notification_template.user_id}", admin_user_path(user_notification_template.user_id) if user_notification_template.user_id.present?
			end
			column :invitee_template
			column :email_due_soon_template
			column :email_overdue_template
			column :email_due_soon_invitee_template
			column :email_overdue_invitee_template
			column :email_report_recipients_template
			column :document_feedback_approved_template
			column :document_feedback_pending_template
			column :document_feedback_rejected_template
			column :created_at
			column :updated_at
		actions
	end
	
	show do
		attributes_table do
			row :id
			row "User Id" do |user_notification_template|
				link_to "User ##{user_notification_template.user_id}", admin_user_path(user_notification_template.user_id) if user_notification_template.user_id.present?
			end
			row :invitee_template
			row :email_due_soon_template
			row :email_overdue_template
			row :email_due_soon_invitee_template
			row :email_overdue_invitee_template
			row :email_report_recipients_template
			row :document_feedback_approved_template
			row :document_feedback_pending_template
			row :document_feedback_rejected_template
			row :created_at
			row :updated_at
		end
		active_admin_comments
	end

	form do |f|
		f.inputs do
	    f.input :user, as: :select2, collection: User.order("id ASC").collect {|user| [user.email, user.id]}, include_blank: 'Select'
			f.input :invitee_template
			f.input :email_due_soon_template
			f.input :email_overdue_template
			f.input :email_due_soon_invitee_template
			f.input :email_overdue_invitee_template
			f.input :email_report_recipients_template
			f.input :document_feedback_approved_template
			f.input :document_feedback_pending_template
			f.input :document_feedback_rejected_template
		end
    f.actions
  end

  	csv do
		column :id
		column "User Id" do |user_notification_template|
			if user_notification_template.user_id.present?
	      a = '=HYPERLINK("link", "user_id")'
	      a = a.gsub("link", admin_user_url(user_notification_template.user_id)).gsub("user_id", "User ##{user_notification_template.user_id}") rescue nil
	    end
		end 
		column :invitee_template
		column :email_due_soon_template
		column :email_overdue_template
		column :email_due_soon_invitee_template
		column :email_overdue_invitee_template
		column :email_report_recipients_template
		column :document_feedback_approved_template
		column :document_feedback_pending_template
		column :document_feedback_rejected_template
		column :created_at
		column :updated_at
  end
end
