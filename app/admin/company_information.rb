ActiveAdmin.register CompanyInformation do
	menu false
	
	permit_params :user_id, :name, :city, :state, :country, :zip_code, :organization_type, :number_of_employees, :logo

	filter :user, as: :select, collection: ->{User.with_role(:company).order("id DESC").collect {|user| [user.email, user.id]}}
	filter :name
	filter :city
	filter :state
	filter :country
	filter :zip_code
	filter :organization_type
	filter :number_of_employees
	filter :created_at, as: :date_range
	filter :updated_at, as: :date_range

	index do 
		selectable_column
		id_column
		column "User Id" do |company_information|
			link_to "User ##{company_information.user_id}", admin_user_path(company_information.user_id) if company_information.user_id.present?
		end 
		column :name
		column :country
		column :state
		column :city
		column :zip_code
		column :organization_type
		column :number_of_employees
		column "Logo" do |company_information|
			image_tag company_information.logo, class: "image-preview-small" if company_information.logo.present? && company_information.logo.file.exists?
		end
		column :created_at
		column :updated_at
    actions
	end

	show do
		attributes_table do
			row :id
			row "User Id" do |company_information|
				link_to "User ##{company_information.user_id}", admin_user_path(company_information.user_id) if company_information.user_id.present?
			end 
			row :name
			row :country
			row :state
			row :city
			row :zip_code
			row :organization_type
			row :number_of_employees
			row "Logo" do |company_information|
				image_tag company_information.logo, class: "image-preview-small" if company_information.logo.present? && company_information.logo.file.exists?
			end
			row :created_at
			row :updated_at
		end
		active_admin_comments
	end

	form do |f|
		f.inputs do
			if f.object.new_record?
				users = User.where(id: params[:user_id])
				if users.present?
					input :user_id, as: :select2, collection: users.collect {|user| [user.email, user.id]}, selected: users.first.id, include_blank: 'Select', input_html: {disabled: true}
				else
					input :user_id, as: :select2, collection: User.managers_without_company_information.collect {|user| [user.email, user.id]}, include_blank: 'Select'
				end
			else
				users = User.where(id: f.object.user_id)
				input :user_id, as: :select2, collection: users.collect {|user| [user.email, user.id]}, include_blank: 'Select', input_html: {disabled: true}
			end
			input :name
			input :country, as: :select2, collection: CS.countries.map {|c_key, c_value| [c_value, c_key]}, include_blank: 'Select', input_html: { onchange: remote_request(:post, :change_country, {country_id: "$('#company_information_country').val()"}, :company_information_state) + "$('#company_information_city').html('');" }
			if f.object.new_record?
				input :state, as: :select2, include_blank: 'Select', input_html: { onchange: remote_request(:post, :change_state, {country_id: "$('#company_information_country').val()", state_id: "$('#company_information_state').val()"}, :company_information_city) }
				input :city, as: :select2, include_blank: 'Select'
			else
				input :state, as: :select2, collection: CS.states(f.object.country).map {|s_key, s_value| [s_value, s_key]}, include_blank: 'Select', input_html: { onchange: remote_request(:post, :change_state, {country_id: "$('#company_information_country').val()", state_id: "$('#company_information_state').val()"}, :company_information_city) }
				input :city, as: :select2, collection: CS.cities(f.object.state, f.object.country).map {|city| [city, city]}, include_blank: 'Select'
			end
			input :zip_code
			input :organization_type, as: :select2, collection: OrganizationType.select(:id, :org_type).collect {|organization_type| [organization_type.org_type, organization_type.org_type]}, include_blank: 'Select'
			input :number_of_employees, as: :select2, collection: NumberOfEmployee.select(:id, :employees).collect {|number_of_employee| [number_of_employee.employees, number_of_employee.employees]}, include_blank: 'Select'
			input :logo, as: :file, image_preview: true
		end
    f.actions
  end

  csv do
		column :id
		column "User Id" do |company_information|
			if company_information.user_id.present?
	      a = '=HYPERLINK("link", "user_id")'
	      a = a.gsub("link", admin_user_url(company_information.user_id)).gsub("user_id", "User ##{company_information.user_id}") rescue nil
	    end
		end 
		column :name
		column :country
		column :state
		column :city
		column :zip_code
		column :organization_type
		column :number_of_employees
		column "Logo" do |company_information|
			if company_information.logo.present? && company_information.logo.file.exists?
	      a = '=HYPERLINK("link", "logo_identifier")'
	      a = a.gsub("link", ENV["DOMAIN"] + company_information.logo.url).gsub("logo_identifier", company_information.logo_identifier) rescue nil
	    end
		end
		column :created_at
		column :updated_at
  end

  collection_action :change_country, method: :post do
		country_id = params[:country_id].present? ? params[:country_id] : ""
		states = CS.states(country_id)
		state_text = "<option value=''>Select</option>"
		states.map do |s_key, s_value|
			state_text += "<option value='#{s_key}'>#{s_value}</option>\n"
		end
    render text: state_text
  end

  member_action :change_country, method: :post do
		country_id = params[:country_id].present? ? params[:country_id] : ""
		states = CS.states(country_id)
		state_text = "<option value=''>Select</option>"
		states.map do |s_key, s_value|
			state_text += "<option value='#{s_key}'>#{s_value}</option>\n"
		end
    render text: state_text
  end

  collection_action :change_state, method: :post do
		state_id = params[:state_id].present? ? params[:state_id] : ""
		country_id = params[:country_id].present? ? params[:country_id] : ""
		cities = CS.cities(state_id, country_id)
		city_text = "<option value=''>Select</option>"
		cities.map do |city|
			city_text += "<option value='#{city}'>#{city}</option>\n"
		end
    render text: city_text
  end

  member_action :change_state, method: :post do
		state_id = params[:state_id].present? ? params[:state_id] : ""
		country_id = params[:country_id].present? ? params[:country_id] : ""
		cities = CS.cities(state_id, country_id)
		city_text = "<option value=''>Select</option>"
		cities.map do |city|
			city_text += "<option value='#{city}'>#{city}</option>\n"
		end
    render text: city_text
  end
end
