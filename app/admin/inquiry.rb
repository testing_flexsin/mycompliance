ActiveAdmin.register Inquiry do
	menu false
	
	permit_params :name, :email, :contact_number, :message
end
