ActiveAdmin.register Comment, as: "News Comments" do
	menu false
	
	permit_params :news_id, :parent_id, :user_name, :email, :message

	remove_filter :ancestor_hierarchies, :descendant_hierarchies
	filter :news, as: :select, collection: ->{News.order("id DESC").collect {|news| [news.title, news.id] }}
	filter :parent
	filter :user_name
	filter :email
	filter :message
	filter :created_at, as: :date_range
	filter :updated_at, as: :date_range

	form do |f|
		f.inputs do
	    f.input :news
	    f.input :parent, as: :select, collection: Comment.order("id ASC").collect {|comment| [comment.parent_id, comment.id] }
			f.input :user_name
			f.input :email
			f.input :message
		end
		f.actions
  end

  csv do
		column :id
    column :user_name
		column :email
		column :message
		column :created_at
		column :updated_at
  end
end
