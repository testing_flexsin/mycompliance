ActiveAdmin.register UserEmailNotification do
	menu false
	
	permit_params :id, :user_id, :due_soon, :due_soon_often, :overdue, :overdue_often, :due_soon_invitee, :due_soon_often_invitee, :overdue_invitee, :overdue_often_invitee

	filter :user, as: :select, collection: ->{User.with_any_role(:company, :manager).collect {|user| [user.email, user.id]}}
	filter :due_soon
	filter :due_soon_often
	filter :overdue
	filter :overdue_often
	filter :due_soon_invitee
	filter :due_soon_often_invitee
	filter :overdue_invitee
	filter :overdue_often_invitee
	filter :created_at, as: :date_range
	filter :updated_at, as: :date_range

	index do 
		selectable_column
		id_column
		column "User Id" do |user_email_notification|
			link_to "User ##{user_email_notification.user_id}", admin_user_path(user_email_notification.user_id) if user_email_notification.user_id.present?
		end
		column :due_soon
		column :due_soon_often
		column :overdue
		column :overdue_often
		column :due_soon_invitee
		column :due_soon_often_invitee
		column :overdue_invitee
		column :overdue_often_invitee
		column :created_at
		column :updated_at
		actions
	end

	show do
		attributes_table do
			row :id
			row "User Id" do |user_email_notification|
				link_to "User ##{user_email_notification.user_id}", admin_user_path(user_email_notification.user_id) if user_email_notification.user_id.present?
			end
			row :due_soon
			row :due_soon_often
			row :overdue
			row :overdue_often
			row :due_soon_invitee
			row :due_soon_often_invitee
			row :overdue_invitee
			row :overdue_often_invitee
			row :created_at
			row :updated_at
		end
		active_admin_comments
	end

	form do |f|
		f.inputs do
	    f.input :user, as: :select2, collection: User.with_any_role(:company, :manager).collect {|user| [user.email, user.id]}, include_blank: 'Select'
			input :due_soon
			input :due_soon_often
			input :overdue
			input :overdue_often
			input :due_soon_invitee
			input :due_soon_often_invitee
			input :overdue_invitee
			input :overdue_often_invitee
		end
    f.actions
  end

  csv do
		column :id
		column "User Id" do |user_email_notification|
			if user_email_notification.user_id.present?
	      a = '=HYPERLINK("link", "user_id")'
	      a = a.gsub("link", admin_user_url(user_email_notification.user_id)).gsub("user_id", "User ##{user_email_notification.user_id}") rescue nil
	    end
		end 
		column :due_soon
		column :due_soon_often
		column :overdue
		column :overdue_often
		column :due_soon_invitee
		column :due_soon_often_invitee
		column :overdue_invitee
		column :overdue_often_invitee
		column :created_at
		column :updated_at
  end
	
end
