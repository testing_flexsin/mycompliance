ActiveAdmin.register UserPreferredEmail do
	menu false
	
	permit_params :id, :user_id, :first_name, :last_name, :full_name, :email, :pause

	filter :user, as: :select, collection: ->{User.with_any_role(:company, :manager).collect {|user| [user.email, user.id]}}
	filter :email
	filter :full_name
	filter :pause
	filter :created_at, as: :date_range
	filter :updated_at, as: :date_range

	index do 
		selectable_column
		id_column
		column "User Id" do |user_preferred_email|
			link_to "User ##{user_preferred_email.user_id}", admin_user_path(user_preferred_email.user_id) if user_preferred_email.user_id.present?
		end 
		column :email
		column :full_name
		column :pause
		column :created_at
		column :updated_at
		actions
	end

	form do |f|
		f.inputs do
	    f.input :user, as: :select2, collection: User.with_any_role(:company, :manager).collect {|user| [user.email, user.id]}, include_blank: 'Select'
			f.input :email
			f.input :full_name
			f.input :pause
		end
    f.actions
  end
	
	show do
		attributes_table do
			row :id
			row "User Id" do |user_preferred_email|
				link_to "User ##{user_preferred_email.user_id}", admin_user_path(user_preferred_email.user_id) if user_preferred_email.user_id.present?
			end 
			row :email
			row :full_name
			row :pause
			row :created_at
			row :updated_at
		end
		active_admin_comments
	end

	csv do
		column :id
		column "User Id" do |user_preferred_email|
			if user_preferred_email.user_id.present?
	      a = '=HYPERLINK("link", "user_id")'
	      a = a.gsub("link", admin_user_url(user_preferred_email.user_id)).gsub("user_id", "User ##{user_preferred_email.user_id}") rescue nil
	    end
		end 
		column :email
		column :full_name
		column :pause
		column :created_at
		column :updated_at
  end
end
