ActiveAdmin.register ContactUs do
	menu false
	
	permit_params :name, :email, :contact_number, :message
	
	filter :name
	filter :email
	filter :contact_number
	filter :message
	filter :created_at
	filter :updated_at

	index do 
		selectable_column
		id_column
		column :name
		column :email
		column :contact_number
		column :message
		column :created_at
		column :updated_at
		actions
	end

	show do
		attributes_table do
			row :id
			row :name
			row :email
			row :contact_number
			row :message
			row :created_at
			row :updated_at
		end
		active_admin_comments
	end

	form do |f|
		f.inputs do
	    f.input :name
			f.input :email
			f.input :contact_number
			f.input :message
		end
    f.actions
  end

	csv do
		column :id
		column :name
		column :email
		column :contact_number
		column :message
		column :created_at
		column :updated_at
  end
end
