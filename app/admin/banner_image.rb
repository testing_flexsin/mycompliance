ActiveAdmin.register BannerImage do
	menu false
	
	permit_params :image, :description, :active, :heading, :link
	
	index do 
	selectable_column
		id_column
		column :description
		column "Image" do |banner_information|
  		image_tag banner_information.image, class: "image-preview-small"
  	end
		column :active
		column :heading
		column :link
		column :created_at
		column :updated_at
		actions
	end

	show do
		attributes_table do
			row :id
			row :description
			row "Image" do |banner_information|
				image_tag banner_information.image, class: "image-preview-small"
			end
			row :active
			row :heading
			row :link
			row :created_at
			row :updated_at
		end
		active_admin_comments
	end

	csv do
		column :id
    column :description
		column "Image" do |banner_information|
  		image_tag banner_information.image_identifier
  	end
		column :active
		column :heading
		column :link
		column :created_at
		column :updated_at
  end

end
