ActiveAdmin.register User do
	menu false
	
	permit_params :email, :first_name, :last_name, :user_name, :password, :password_confirmation, :avatar, :job_title, :mobile_number, :landline_number, :time_zone, :active, :admin

	filter :email
	filter :roles
	filter :first_name
	filter :last_name
	filter :user_name
	filter :job_title
	filter :mobile_number
	filter :landline_number
	filter :time_zone
	filter :active
	filter :managers, as: :select, collection: ->{User.with_role(:manager).order("id DESC").collect {|user| [user.email, user.id] }}
	filter :admin
	filter :created_at, as: :date_range
	filter :updated_at, as: :date_range
	filter :deleted_at, as: :date_range
	
	config.clear_action_items!

	action_item only: :index do
    link_to "New Company User", new_admin_user_path
	end
	
	action_item only: :show do
		link_to("Edit User", edit_admin_user_path(user)) + " " + link_to("Delete User", admin_user_path(user), method: :delete, data: {confirm: "Are you sure you want to delete this?"})
	end
	
	action_item only: :edit do
    link_to "Delete User", admin_user_path(user), method: :delete, data: {confirm: "Are you sure you want to delete this?"}
	end
	
	after_create do |user|
		user.add_role :company
		password = params[:user][:password] rescue ""
		begin    
    	UserMailer.account_created(user, password).deliver_now
    rescue => e
      Rails.logger.info "UserMailer - account_created: #{e}"
    end
  end
	
	before_save do |user|
    unless user.new_record?
      user = User.find(params[:id]) rescue ""
      password = params[:user][:password] rescue ""
      if user.present? && password.present? && !user.valid_password?(password)
      	begin    
        	UserMailer.password_updated(user, password).deliver_now
    		rescue => e
      		Rails.logger.info "UserMailer - password_updated: #{e}"
    		end
      end
    end
  end

	index(row_class: -> user { 'deleted' if user.deleted? }) do
		selectable_column
		id_column
		column :email
    column "Role" do |user|
      role = user.roles.pluck(:name).uniq.join(",")
      role.titleize
    end
		column :first_name
		column :last_name
		column :user_name
		column "Avatar" do |user|
	  	image_tag user.avatar, class: "image-preview-small" if user.avatar.present? && user.avatar.file.exists?
	  end
		column :job_title
		column :mobile_number
		column :landline_number
		column :time_zone
		column :active
		column :admin
		column :created_at
		column :updated_at
		column :deleted_at
		actions defaults: false do |user|
			user_roles = user.roles.pluck(:name)
			link_to("View", admin_user_path(user)) + " " + link_to("Edit", edit_admin_user_path(user)) + " " + ((user_roles.include? 'company') ? (user.company_information.present? ? link_to("Company Profile", admin_company_information_path(user.company_information.id)) : link_to("Create Company Profile", new_admin_company_information_path(user_id: user.id))) : (((user_roles.include? 'manager') && user.company_id.present?) ? link_to("Company User", admin_user_path(user.company_id)) : '')) + (user.deleted? ? link_to("Restore", restore_admin_user_path(user), data: {confirm: "Are you sure you want to restore this?"}) + " " + link_to("Really Delete", really_delete_admin_user_path(user),method: :delete, data: {confirm: "Are you sure you want to Really delete this?"}) : link_to("Really Delete", really_delete_admin_user_path(user), method: :delete, data: {confirm: "Are you sure you want to really delete this?"})+ " " + link_to("Soft Delete", admin_user_path(user), method: :delete, data: {confirm: "Are you sure you want to soft delete this?"})) + (user.confirmed? ? "" : link_to("Confirm User", confirm_admin_user_path(user), data: {confirm: "Are you sure you want to Confirm this?"}))
		end
	end

	show do |user|
		user_roles = user.roles.pluck(:name)
		attributes_table do
			row :id
			row :email
	    row "Role" do |user|
	      role = user_roles.uniq.join(",")
	      role.titleize
	    end
			row :first_name
			row :last_name
			row :user_name
			row "Avatar" do |user|
		  	image_tag user.avatar, class: "image-preview" if user.avatar.present? && user.avatar.file.exists?
		  end
			row :job_title
			row :mobile_number
			row :landline_number
			row :time_zone
			row :active
			row :admin
			if user_roles.include? 'company'
				row "Company Profile" do |user|
					company_information = user.company_information
					if company_information.present?
			  		link_to("Company Profile", admin_company_information_path(company_information.id))
					else
						link_to("Create Company Profile", new_admin_company_information_path(user_id: user.id))
					end
			  end
			end
			if user_roles.include? 'manager'
				row "Company User" do |user|
			  	link_to("Company User", admin_user_path(user.company_id)) if user.company_id.present?
			  end
			end
			if !user.confirmed? 
				row "Confirm User" do |user|
					link_to("Confirm User", confirm_admin_user_path(user), data: {confirm: "Are you sure you want to Confirm this?"})
				end
			end
			row :created_at
			row :updated_at
			row :deleted_at
		end
		
		if user_roles.include? 'company'
			panel "Manager Users" do
	      table_for(user.managers.with_deleted) do |user|
					column :id
					column :email
					column :first_name
					column :last_name
					column :user_name
					column "Avatar" do |user|
				  	image_tag user.avatar, class: "image-preview-small" if user.avatar.present? && user.avatar.file.exists?
				  end
					column :job_title
					column :mobile_number
					column :landline_number
					column :time_zone
					column :active
					column :admin
					column :created_at
					column :updated_at
					column :deleted_at
					column "" do |user|
	          link_to("View", admin_user_path(user)) + "&nbsp;&nbsp;&nbsp;&nbsp;".html_safe + link_to("Edit", edit_admin_user_path(user)) + "&nbsp;&nbsp;&nbsp;&nbsp;".html_safe + (user.deleted? ? link_to("Restore", restore_admin_user_path(user), data: {confirm: "Are you sure you want to restore this?"}) + "&nbsp;&nbsp;&nbsp;&nbsp;".html_safe + link_to("Really Delete", really_delete_admin_user_path(user),method: :delete, data: {confirm: "Are you sure you want to Really delete this?"}) : link_to("Really Delete", really_delete_admin_user_path(user), method: :delete, data: {confirm: "Are you sure you want to really delete this?"}) + "&nbsp;&nbsp;&nbsp;&nbsp;".html_safe + link_to("Soft Delete", admin_user_path(user), method: :delete, data: {confirm: "Are you sure you want to soft delete this?"})) + " " + (user.confirmed? ? "" : link_to("Confirm User", confirm_admin_user_path(user), data: {confirm: "Are you sure you want to Confirm this?"}))
	        end  
	      end
	    end
		end
		
		active_admin_comments
	end

	form do |f|
		f.inputs do
			f.input :email
			f.input :first_name
			f.input :last_name
			f.input :user_name
      f.input :password
      f.input :password_confirmation
			f.input :avatar, as: :file, image_preview: true
			f.input :job_title
			f.input :mobile_number
			f.input :landline_number
			f.input :time_zone
			f.input :active
			f.input :admin
		end
    f.actions
  end

  csv do
		column :id
		column :email
    column "Role" do |user|
      role = user.roles.pluck(:name).uniq.join(",")
      role.titleize
    end
		column :first_name
		column :last_name
		column :user_name
		column "Avatar" do |user|
			if user.avatar.present? && user.avatar.file.exists?
	      a = '=HYPERLINK("link", "avatar_identifier")'
	      a = a.gsub("link", ENV["DOMAIN"] + user.avatar.url).gsub("avatar_identifier", user.avatar_identifier) rescue nil
	    end
	  end
		column :job_title
		column :mobile_number
		column :landline_number
		column :time_zone
		column :active
		column :admin
		column :created_at
		column :updated_at
		column :deleted_at
  end


	controller do
		def scoped_collection
      User.with_deleted
    end

		def find_resource
		  begin
		    scoped_collection.friendly.find(params[:id])
		  rescue ActiveRecord::RecordNotFound
		    scoped_collection.find(params[:id])
		  end
		end
		
    def update
      if params[:user][:password].blank?
        params[:user].delete("password")
        params[:user].delete("password_confirmation")
      end
      super
    end
	end

	member_action :restore do
		user = User.restore(params[:id]) rescue ""
		message = user.present? ? "User Restored successfully" : "Request failed, something went wrong."
		redirect_to :back, notice: message
	end

	member_action :confirm do
		confirmed = User.find(params[:id]).confirm rescue ""
		message = confirmed ? "User Confirmed successfully" : "Request failed, something went wrong."
		redirect_to :back, notice: message
	end

	member_action :really_delete, method: :delete do
		user = User.with_deleted.find(params[:id]) rescue ""
		if user.present?
			user.really_destroy!
			redirect_to :back, notice: "User Really Deleted successfully"
		else
			redirect_to :back, error: "Request failed, something went wrong."
		end
	end
end
