ActiveAdmin.register EmailTemplate do
	menu false
	
	permit_params :admin_email, :user_confirmation_instruction, :user_invitation_instruction, :user_password_change, :user_password_update, :user_reset_password_instruction, :user_inquiry, :user_newsletter, :user_contact_us, :admin_confirmation_instruction, :admin_password_updated, :admin_account_created, :admin_reset_password_instruction, :admin_inquiry, :admin_newsletter, :admin_contact_us, :manager_account, :manager_created, :manager_updated, :invitation_confirmation, :account_created, :password_updated
	
	filter :admin_email
	filter :user_confirmation_instruction
	filter :user_invitation_instruction
	filter :user_password_change
	filter :user_password_update
	filter :user_reset_password_instruction
	filter :user_inquiry
	filter :user_newsletter
	filter :user_contact_us
	filter :admin_confirmation_instruction
	filter :admin_password_updated
	filter :admin_account_created
	filter :admin_reset_password_instruction
	filter :admin_inquiry
	filter :admin_newsletter
	filter :admin_contact_us
	filter :manager_account
	filter :manager_created
	filter :manager_updated
	filter :invitation_confirmation
	filter :account_created
	filter :password_updated
	filter :created_at, as: :date_range
	filter :updated_at, as: :date_range

	index do 
		selectable_column
		id_column
		column :admin_email
		column :user_confirmation_instruction
		column :user_invitation_instruction
		column :user_password_change
		column :user_password_update
		column :user_reset_password_instruction
		column :user_inquiry
		column :user_newsletter
		column :user_contact_us
		column :admin_confirmation_instruction
		column :admin_password_updated
		column :admin_account_created
		column :admin_reset_password_instruction
		column :admin_inquiry
		column :admin_newsletter
		column :admin_contact_us
		column :manager_account
		column :manager_created
		column :manager_updated
		column :invitation_confirmation
		column :account_created
		column :password_updated
		column :created_at
		column :updated_at
		actions
	end

	show do
		attributes_table do
			row :id
			row :admin_email
	  	row :user_confirmation_instruction
	  	row :user_invitation_instruction
	  	row :user_password_change
	  	row :user_password_update
	  	row :user_reset_password_instruction
	  	row :user_inquiry
	  	row :user_newsletter
	  	row :user_contact_us
	  	row :admin_confirmation_instruction
	  	row :admin_password_updated
	  	row :admin_account_created
	  	row :admin_reset_password_instruction
	  	row :admin_inquiry
	  	row :admin_newsletter
	  	row :admin_contact_us
	  	row :manager_account
	  	row :manager_created
	  	row :manager_updated
	  	row :invitation_confirmation
			row :account_created
			row :password_updated
			row :created_at
			row :updated_at
		end
		active_admin_comments
	end

	csv do
		column :id
		column :admin_email
  	column :user_confirmation_instruction
  	column :user_invitation_instruction
  	column :user_password_change
  	column :user_password_update
  	column :user_reset_password_instruction
  	column :user_inquiry
  	column :user_newsletter
  	column :user_contact_us
  	column :admin_confirmation_instruction
  	column :admin_password_updated
  	column :admin_account_created
  	column :admin_reset_password_instruction
  	column :admin_inquiry
  	column :admin_newsletter
  	column :admin_contact_us
  	column :manager_account
  	column :manager_created
  	column :manager_updated
  	column :invitation_confirmation
		column :account_created
		column :password_updated
		column :created_at
		column :updated_at
  end	
end


