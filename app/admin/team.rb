ActiveAdmin.register Team do
	menu false
	
	permit_params :name, :image, :description, :show_on_home, :facebook, :twitter, :linkedin, :gmail
	
	filter :name
	filter :description
	filter :facebook
	filter :twitter
	filter :linkedin
	filter :gmail
	filter :show_on_home
	filter :created_at, as: :date_range
	filter :updated_at, as: :date_range

	index do 
		selectable_column
		id_column
		column :name
		column "Image" do |team|
  		image_tag team.image, class: "image-preview-small" if team.image.present? && team.image.file.exists?
  	end
		column :description
		column :facebook
		column :twitter
		column :linkedin
		column :gmail
		column :show_on_home
		column :created_at
		column :updated_at
		actions
	end

	show do
		attributes_table do
			row :id
			row :name
			row "Image" do |team|
				image_tag team.image, class: "image-preview-small" if team.image.present? && team.image.file.exists?
			end
			row :description
			row :facebook
			row :twitter
			row :linkedin
			row :gmail
			row :show_on_home
			row :created_at
			row :updated_at
		end
		active_admin_comments
	end

	form do |f|
		f.inputs do
	    f.input :name
			f.input :image, as: :file, image_preview: true
			f.input :description
			f.input :facebook
			f.input :twitter
			f.input :linkedin
			f.input :gmail
			f.input :show_on_home
		end
    f.actions
  end

  csv do
		column :id
		column :name
		column "Image" do |team|
			if team.image.present? && team.image.file.exists?
	      a = '=HYPERLINK("link", "image_identifier")'
	      a = a.gsub("link", ENV["DOMAIN"] + team.image.url).gsub("image_identifier", team.image_identifier) rescue nil
	    end
  	end
		column :description
		column :facebook
		column :twitter
		column :linkedin
		column :gmail
		column :show_on_home
		column :created_at
		column :updated_at
  end
	
	controller do
		def find_resource
		  begin
		    scoped_collection.friendly.find(params[:id])
		  rescue ActiveRecord::RecordNotFound
		    scoped_collection.find(params[:id])
		  end
		end
	end
end
