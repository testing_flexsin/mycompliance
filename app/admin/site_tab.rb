ActiveAdmin.register SiteTab do
	menu false
	
	# permit_params :user_id, :site_id, :name, :items

	filter :user, as: :select, collection: ->{User.with_any_role("company", "manager").collect {|user| [user.email, user.id]}}
	filter :site
	filter :name
	filter :items
	filter :created_at, as: :date_range
	filter :updated_at, as: :date_range

	index do
		selectable_column
		id_column
		column "User Id" do |site_tab|
			link_to "User ##{site_tab.user_id}", admin_user_path(site_tab.user_id) if site_tab.user_id.present?
		end
		column "Site Id" do |site_tab|
			link_to "Site ##{site_tab.site_id}", admin_site_path(site_tab.site_id) if site_tab.site_id.present?
		end
		column :name
		column :items
		column :created_at
		column :updated_at
		actions
	end

	show do
		attributes_table do
			row :id
			column "User Id" do |site_tab|
				link_to "User ##{site_tab.user_id}", admin_user_path(site_tab.user_id) if site_tab.user_id.present?
			end
			column "Site Id" do |site_tab|
				link_to "Site ##{site_tab.site_id}", admin_site_path(site_tab.site_id) if site_tab.site_id.present?
			end
			row :name
			row :items
			row :created_at
			row :updated_at
		end
		active_admin_comments
	end

	csv do
		column :id
		column "User Id" do |site_tab|
			if site_tab.user_id.present?
	      a = '=HYPERLINK("link", "user_id")'
	      a = a.gsub("link", admin_user_url(site_tab.user_id)).gsub("user_id", "User ##{site_tab.user_id}") rescue nil
	    end
		end
		column "Site Id" do |site_tab|
			if site_tab.site_id.present?
	      a = '=HYPERLINK("link", "site_id")'
	      a = a.gsub("link", admin_site_url(site_tab.site_id)).gsub("site_id", "Site ##{site_tab.site_id}") rescue nil
	    end
		end
		column :name
		column :items
		column :created_at
		column :updated_at
  end
	
	actions :index, :show
end


