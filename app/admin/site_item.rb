ActiveAdmin.register SiteItem do
	menu false
	
	# permit_params :user_id, :site_id, :site_tab_id, :title, :frequency, :required_for_site_compliance, :due_date, :due_soon, :status, :compliant, :template

	filter :user, as: :select, collection: ->{User.with_any_role(:company, :manager).collect {|user| [user.email, user.id]}}
	filter :site
	filter :title
	filter :frequency
	filter :required_for_site_compliance
	filter :due_date, as: :date_range
	filter :due_soon
	filter :status
	filter :compliant
	filter :template
	filter :created_at, as: :date_range
	filter :updated_at, as: :date_range

	index do
		selectable_column
		id_column
		column "User Id" do |site_item|
			link_to "User ##{site_item.user_id}", admin_user_path(site_item.user_id) if site_item.user_id.present?
		end
		column "Site Id" do |site_item|
			link_to "Site ##{site_item.site_id}", admin_site_path(site_item.site_id) if site_item.site_id.present?
		end 
		column "Site Tab Id" do |site_item|
			link_to "Site Tab ##{site_item.site_tab_id}", admin_site_tab_path(site_item.site_tab_id) if site_item.site_tab_id.present?
		end 
		column :title
		column :frequency
		column :required_for_site_compliance
		column :due_date
		column :due_soon
		column :status
		column :compliant
		column :template
		column :created_at
		column :updated_at
    actions
	end

	show do
		attributes_table do
			row :id
			row "User Id" do |site_item|
				link_to "User ##{site_item.user_id}", admin_user_path(site_item.user_id) if site_item.user_id.present?
			end
			row "Site Id" do |site_item|
				link_to "Site ##{site_item.site_id}", admin_site_path(site_item.site_id) if site_item.site_id.present?
			end 
			row "Site Tab Id" do |site_item|
				link_to "Site Tab ##{site_item.site_tab_id}", admin_site_tab_path(site_item.site_tab_id) if site_item.site_tab_id.present?
			end 
			row :title
			row :frequency
			row :required_for_site_compliance
			row :due_date
			row :due_soon
			row :status
			row :compliant
			row :template
			row :created_at
			row :updated_at
		end
		active_admin_comments
	end

	csv do
		column :id
		column "User Id" do |site_item|
			if site_item.user_id.present?
	      a = '=HYPERLINK("link", "user_id")'
	      a = a.gsub("link", admin_user_url(site_item.user_id)).gsub("user_id", "User ##{site_item.user_id}") rescue nil
	    end
		end
		column "Site Id" do |site_item|
			if site_item.site_id.present?
	      a = '=HYPERLINK("link", "site_id")'
	      a = a.gsub("link", admin_site_url(site_item.site_id)).gsub("site_id", "Site ##{site_item.site_id}") rescue nil
	    end
		end 
		column "Site Tab Id" do |site_item|
			if site_item.site_tab_id.present?
	      a = '=HYPERLINK("link", "site_tab_id")'
	      a = a.gsub("link", admin_site_tab_url(site_item.site_tab_id)).gsub("site_tab_id", "Site Tab ##{site_item.site_tab_id}") rescue nil
	    end
		end 
		column :title
		column :frequency
		column :required_for_site_compliance
		column :due_date
		column :due_soon
		column :status
		column :compliant
		column :template
		column :created_at
		column :updated_at
  end
	
	actions :index, :show
end
