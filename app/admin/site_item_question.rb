ActiveAdmin.register SiteItemQuestion do
	menu false
	
	# permit_params :site_id, :site_item_id, :question, :required_for_compliant

	filter :site
	filter :question
	filter :required_for_compliant
	filter :created_at, as: :date_range
	filter :updated_at, as: :date_range

	index do
		selectable_column
		id_column
		column "Site Id" do |site_item_question|
			link_to "Site ##{site_item_question.site_id}", admin_site_path(site_item_question.site_id) if site_item_question.site_id.present?
		end 
		column "Site Item Id" do |site_item_question|
			link_to "Site Item ##{site_item_question.site_item_id}", admin_site_item_path(site_item_question.site_item_id) if site_item_question.site_item_id.present?
		end 
		column :question
		column :required_for_compliant
		column :created_at
		column :updated_at
    actions
	end

	show do
		attributes_table do
			row :id
			row "Site Id" do |site_item_question|
				link_to "Site ##{site_item_question.site_id}", admin_site_path(site_item_question.site_id) if site_item_question.site_id.present?
			end 
			row "Site Item Id" do |site_item_question|
				link_to "Site Item ##{site_item_question.site_item_id}", admin_site_item_path(site_item_question.site_item_id) if site_item_question.site_item_id.present?
			end 
			row :question
			row :required_for_compliant
			row :created_at
			row :updated_at
		end
		active_admin_comments
	end

	csv do
		column :id
		column "Site Id" do |site_item_question|
			if site_item_question.site_id.present?
	      a = '=HYPERLINK("link", "site_id")'
	      a = a.gsub("link", admin_site_url(site_item_question.site_id)).gsub("site_id", "Site ##{site_item_question.site_id}") rescue nil
	    end
		end 
		column "Site Item Id" do |site_item_question|
			if site_item_question.site_item_id.present?
	      a = '=HYPERLINK("link", "site_item_id")'
	      a = a.gsub("link", admin_site_item_url(site_item_question.site_item_id)).gsub("site_item_id", "Site ##{site_item_question.site_item_id}") rescue nil
	    end
		end 
		column :question
		column :required_for_compliant
		column :created_at
		column :updated_at
  end
	
	actions :index, :show
end
