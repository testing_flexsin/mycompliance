ActiveAdmin.register Site do
	menu false
	
	# permit_params :user_id, :name, :annual_compliance_due, :tabs, :tab_items, :site_item_id, :status, :compliant, :active, :template

	filter :user, as: :select, collection: ->{User.where(id: Site.pluck(:user_id).uniq).order("id DESC").collect {|user| [user.email, user.id]}}
	filter :name
	filter :annual_compliance_due
	filter :tabs
	filter :tab_items
	filter :status
	filter :compliant
	filter :template
	filter :active
	filter :created_at, as: :date_range
	filter :updated_at, as: :date_range

	index do 
		selectable_column
		id_column
		column "User Id" do |site|
			link_to "User ##{site.user_id}", admin_user_path(site.user_id) if site.user_id.present?
		end
		column :name
		column :annual_compliance_due
		column :tabs
		column :tab_items
		column :status
		column :compliant
		column :template
		column :active
		column :created_at
		column :updated_at
    actions
	end
	
	show do
		attributes_table do
			row :id
			row "User Id" do |site|
				link_to "User ##{site.user_id}", admin_user_path(site.user_id) if site.user_id.present?
			end
			row :name
			row :annual_compliance_due
			row :tabs
			row :tab_items
			row :status
			row :compliant
			row :template
			row :active
			row :created_at
			row :updated_at
		end
		active_admin_comments
	end

	csv do
		column :id
    column "User Id" do |site|
    	if site.user_id.present?
	      a = '=HYPERLINK("link", "user_id")'
	      a = a.gsub("link", admin_user_url(site.user_id)).gsub("user_id", "User ##{site.user_id}") rescue nil
	    end
		end
    column :name
		column :annual_compliance_due
		column :tabs
		column :tab_items
		column :status
		column :compliant
		column :template
		column :active
		column :created_at
		column :updated_at
  end
	
	actions :index, :show
end
