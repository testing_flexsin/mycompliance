ActiveAdmin.register SiteItemDocument do
	menu false
	
	# permit_params :user_id, :site_id, :site_item_id, :file, :archive, :automatic_archive, :current, :pending, :approve, :feedback, :contractor_email

	filter :user, as: :select, collection: ->{User.with_any_role(:company, :manager).collect {|user| [user.email, user.id]}}
	filter :site
	filter :site_item
	filter :archive
	filter :automatic_archive
	filter :current
	filter :pending
	filter :approve
	filter :feedback
	filter :contractor_email
	filter :created_at, as: :date_range
	filter :updated_at, as: :date_range

	index do
		selectable_column
		id_column
		column "User Id" do |site_item_document|
			link_to "User ##{site_item_document.user_id}", admin_user_path(site_item_document.user_id) if site_item_document.user_id.present?
		end
		column "Site Id" do |site_item_document|
			link_to "Site ##{site_item_document.site_id}", admin_site_path(site_item_document.site_id) if site_item_document.site_id.present?
		end 
		column "Site Item Id" do |site_item_document|
			link_to "Site Item ##{site_item_document.site_item_id}", admin_site_item_path(site_item_document.site_item_id) if site_item_document.site_item_id.present?
		end
		column "Document" do |site_item_document|
			link_to site_item_document.file_identifier, site_item_document.file.url, download: '' if site_item_document.file.present? && site_item_document.file.file.exists?
		end
		column :archive
		column :automatic_archive
		column :current
		column :pending
		column :approve
		column :feedback
		column :contractor_email
		column :created_at
		column :updated_at
    actions
	end

	show do
		attributes_table do
			row :id
			row "User Id" do |site_item_document|
				link_to "User ##{site_item_document.user_id}", admin_user_path(site_item_document.user_id) if site_item_document.user_id.present?
			end
			row "Site Id" do |site_item_document|
				link_to "Site ##{site_item_document.site_id}", admin_site_path(site_item_document.site_id) if site_item_document.site_id.present?
			end 
			row "Site Item Id" do |site_item_document|
				link_to "Site Item ##{site_item_document.site_item_id}", admin_site_item_path(site_item_document.site_item_id) if site_item_document.site_item_id.present?
			end
			row "Document" do |site_item_document|
				link_to site_item_document.file_identifier, site_item_document.file.url, download: '' if site_item_document.file.present? && site_item_document.file.file.exists?
			end
			row :archive
			row :automatic_archive
			row :current	
			row :pending
			row :approve
			row :feedback
			row :contractor_email
			row :created_at
			row :updated_at
		end
		active_admin_comments
	end

	csv do
		column :id
		column "User Id" do |site_item_document|
			if site_item_document.user_id.present?
	      a = '=HYPERLINK("link", "user_id")'
	      a = a.gsub("link", admin_user_url(site_item_document.user_id)).gsub("user_id", "User ##{site_item_document.user_id}") rescue nil
	    end
		end
		column "Site Id" do |site_item_document|
			if site_item_document.site_id.present?
	      a = '=HYPERLINK("link", "site_id")'
	      a = a.gsub("link", admin_site_url(site_item_document.site_id)).gsub("site_id", "Site ##{site_item_document.site_id}") rescue nil
	    end
		end 
		column "Site Item Id" do |site_item_document|
			if site_item_document.site_item_id.present?
	      a = '=HYPERLINK("link", "site_item_id")'
	      a = a.gsub("link", admin_site_item_url(site_item_document.site_item_id)).gsub("site_item_id", "Site Item ##{site_item_document.site_item_id}") rescue nil
	    end
		end
		column "Document" do |site_item_document|
			if site_item_document.file.present? && site_item_document.file.file.exists?
	      a = '=HYPERLINK("link", "file_identifier")'
	      a = a.gsub("link", ENV["DOMAIN"] + site_item_document.file.url).gsub("file_identifier", site_item_document.file_identifier) rescue nil
	    end
		end
		column :archive
		column :automatic_archive
		column :current
		column :pending
		column :approve
		column :feedback
		column :contractor_email
		column :created_at
		column :updated_at
  end
	
	actions :index, :show
end
