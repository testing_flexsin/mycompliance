ActiveAdmin.register Testimonial do
	menu false
	
	permit_params :name, :image, :message, :location, :active
	
	filter :name
	filter :message
	filter :location
	filter :active
	filter :created_at, as: :date_range
	filter :updated_at, as: :date_range

	index do 
		selectable_column
		id_column
		column :name
		column "Image" do |testimonial|
  		image_tag testimonial.image, class: "image-preview-small" if testimonial.image.present? && testimonial.image.file.exists?
  	end
		column :message
		column :location
		column :active
		column :created_at
		column :updated_at
		actions
	end

	show do
		attributes_table do
			row :id
			row :name
			row "Image" do |testimonial|
				image_tag testimonial.image, class: "image-preview-small" if testimonial.image.present? && testimonial.image.file.exists?
			end
			row :message
			row :location
			row :active
			row :created_at
			row :updated_at
		end
		active_admin_comments
	end
	
	form do |f|
		f.inputs do
      f.input :name
			f.input :image, as: :file, image_preview: true
			f.input :message
			f.input :location
			f.input :active
		end
    f.actions
  end

	csv do
		column :id
		column :name
		column "Image" do |testimonial|
			if testimonial.image.present? && testimonial.image.file.exists?
	      a = '=HYPERLINK("link", "image_identifier")'
	      a = a.gsub("link", ENV["DOMAIN"] + testimonial.image.url).gsub("image_identifier", testimonial.image_identifier) rescue nil
	    end
  	end
		column :message
		column :location
		column :active
		column :created_at
		column :updated_at
  end
end
