ActiveAdmin.register AdminUser do
	menu false
  
  permit_params :email, :password, :password_confirmation, :name, :job_title, :phone_number, :active, :permissions

  filter :email
  filter :roles
  filter :name
  filter :job_title
  filter :phone_number
  filter :active
  filter :created_at
  filter :updated_at
  
  before_create do |admin_user|
    admin_user.skip_confirmation_notification!
  end
  
  after_create do |admin_user|
    password = params[:admin_user][:password] rescue ""
    begin
      AdminMailer.confirmation_instructions(admin_user).deliver_now
      AdminMailer.account_created(admin_user, password).deliver_now      
    rescue => e
      Rails.logger.info "AdminMailer - confirmation_instructions, account_created: #{e}"
    end
  end
  
  before_save do |admin_user|
    unless admin_user.new_record?
      admin_user = AdminUser.find(params[:id]) rescue ""
      password = params[:admin_user][:password] rescue ""
      if admin_user.present? && password.present? && !admin_user.valid_password?(password)
        begin
          AdminMailer.password_updated(admin_user, password).deliver_now
        rescue => e
          Rails.logger.info "AdminMailer - password_updated: #{e}"
        end
      end
    end
  end
  
  after_save do |admin_user|
    if params[:admin_user].has_key?(:sub_admin) && (params[:admin_user][:sub_admin] == "1")
      admin_user.roles.destroy_all
      admin_user.add_role :sub_admin
    else
      admin_user.roles.destroy_all
      admin_user.add_role :super_admin
    end
  end
  
  index do
    selectable_column
    id_column
    column :email
    column "Role" do |admin_user|
      role = admin_user.roles.pluck(:name).uniq.join(",")
      role.titleize
    end
    column :name
    column :job_title
    column :phone_number
    column :active
    actions
  end

  form do |f|
    f.inputs "Admin Details" do
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :name
      f.input :job_title
      f.input :phone_number
      f.input :active
      f.input :sub_admin, as: :boolean, input_html: { checked: (admin_user.new_record? ? false : (admin_user.has_role?(:sub_admin) ? true : false)) }
      f.input :permissions, as: :select2_multiple, collection: (ActiveAdmin.application.namespaces[:admin].resources.keys.map(&:name).sort - ["AdminUser", "Dashboard", "NewsComments"]), selected: (admin_user.permissions.present? ? admin_user.permissions.split(",") : ""), include_blank: true, multiple: true
    end
    f.actions
  end

  csv do
    column :id
    column :email
    column "Role" do |admin_user|
      role = admin_user.roles.pluck(:name).uniq.join(",")
      role.titleize
    end
    column :name
    column :job_title
    column :phone_number
    column :active
  end
	
	controller do
    def find_resource
      begin
        scoped_collection.friendly.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        scoped_collection.find(params[:id])
      end
    end
    
    def create
      permissions = params[:admin_user][:permissions].reject!(&:empty?).join(",") rescue ""
      params[:admin_user][:permissions] = permissions
      super
    end
    
    def update
      permissions = params[:admin_user][:permissions].reject!(&:empty?).join(",") rescue ""
      params[:admin_user][:permissions] = permissions
      if params[:admin_user].has_key?(:password) && params[:admin_user][:password].blank?
        params[:admin_user].delete("password")
        params[:admin_user].delete("password_confirmation")
      end
      super
    end
	end
end
