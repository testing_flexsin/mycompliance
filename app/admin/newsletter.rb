ActiveAdmin.register Newsletter do
	menu false
	
	permit_params :name, :email
end
