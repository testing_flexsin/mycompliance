ActiveAdmin.register StaticPage do
	menu false
	
	permit_params :logo, :header_logged_in, :header_not_logged_in, :footer, :features, :about_us, :terms_and_conditions, :privacy_policy, :faq, :contact_us, :services, :pricing, :news, :team, :testimonials

	filter :header_logged_in
	filter :header_not_logged_in
	filter :footer
	filter :features
	filter :about_us
	filter :terms_and_conditions
	filter :privacy_policy
	filter :faq
	filter :contact_us
	filter :services
	filter :pricing
	filter :news
	filter :team
	filter :testimonials
	filter :created_at, as: :date_range
	filter :updated_at, as: :date_range

	index do 
		selectable_column
		id_column
		column "Logo" do |static_page|
  		image_tag static_page.logo, class: "image-preview-small" if static_page.logo.present? && static_page.logo.file.exists?
  	end
		column :header_logged_in
		column :header_not_logged_in
		column :footer
		column :features
		column :about_us
		column :terms_and_conditions
		column :privacy_policy
		column :faq
		column :contact_us
		column :services
		column :pricing
		column :news
		column :team
		column :testimonials
		column :created_at
		column :updated_at
    actions
	end

	show do
		attributes_table do
			row :id
			row "Logo" do |static_page|
	  		image_tag static_page.logo, class: "image-preview-small" if static_page.logo.present? && static_page.logo.file.exists?
	  	end
			row :header_logged_in
			row :header_not_logged_in
			row :footer
			row :features
			row :about_us
			row :terms_and_conditions
			row :privacy_policy
			row :faq
			row :contact_us
			row :services
			row :pricing
			row :news
			row :team
			row :testimonials
			row :created_at
			row :updated_at
		end
		active_admin_comments
	end

	form do |f|
		f.inputs do
			f.input :image, as: :logo, image_preview: true
			f.input :header_logged_in
			f.input :header_not_logged_in
			f.input :footer
			f.input :features
			f.input :about_us
			f.input :terms_and_conditions
			f.input :privacy_policy
			f.input :faq
			f.input :contact_us
			f.input :services
			f.input :pricing
			f.input :news
			f.input :team
			f.input :testimonials
		end
    f.actions
  end

	csv do
		column :id
		column "Logo" do |static_page|
			if static_page.logo.present? && static_page.logo.file.exists?
	      a = '=HYPERLINK("link", "logo_identifier")'
	      a = a.gsub("link", ENV["DOMAIN"] + static_page.logo.url).gsub("logo_identifier", static_page.logo_identifier) rescue nil
	    end
  	end
		column :header_logged_in
		column :header_not_logged_in
		column :footer
		column :features
		column :about_us
		column :terms_and_conditions
		column :privacy_policy
		column :faq
		column :contact_us
		column :services
		column :pricing
		column :news
		column :team
		column :testimonials
		column :created_at
		column :updated_at
  end
end
