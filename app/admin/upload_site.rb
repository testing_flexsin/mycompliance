ActiveAdmin.register_page "Upload Site" do
	menu false

  content do
    render partial: 'upload_site'
  end

  page_action :import_csv, method: :post do
  	begin
	  	file = params[:site]
	  	if file.content_type == "text/csv" && file.present?
	      CSV.foreach(file.path, headers: true) do |row|
		  			site_id = Site.last.id
		  			id = site_id+1
		  			row[:id] = id
		      	Site.create! row.to_hash
	      end
	    	redirect_to admin_upload_site_path, notice: "Site CSV imported successfully!"
	    else
	    	redirect_to admin_upload_site_path, alert: "Please select CSV file only!"
	    end
	  rescue => e
	  	redirect_to admin_upload_site_path, alert: e
	  end
  end

  page_action :site_item_tab_import_csv, method: :post do
  	begin
	  	file = params[:site]
	  	if file.content_type == "text/csv" && file.present?
	      CSV.foreach(file.path, headers: true) do |row|
	  			site = Site.find_by(id: row["site_id"])
	  			if site.present?
		  			user_id = site.user.id
		      	site_tab = SiteTab.where("name LIKE ?", "#{row['site_tab_name']}").last
		      	if site_tab.blank?
		      		site_tab = SiteTab.create(user_id: user_id, site_id: site.id, name: row["site_tab_name"], items: row["site_tab_items"])
		      	end
		      	site_item = SiteItem.create(user_id: user_id, site_id: site.id, site_tab_id: site_tab.id, title: row["site_item_title"], frequency: row["site_item_frequency"], required_for_site_compliance: row["site_item_required_for_site_compliance"], due_date: row["site_item_due_date"], due_soon: row["site_item_due_soon"], status: row["site_item_status"], compliant: row["site_item_compliant"], template: row["site_item_template"])
		      end
	      end
	    	redirect_to admin_upload_site_path, notice: "Site Item & Tab CSV imported successfully!"
	    else
	    	redirect_to admin_upload_site_path, alert: "Please select CSV file only!"
	    end
	  rescue => e
	  	redirect_to admin_upload_site_path, alert: e
	  end
  end

end