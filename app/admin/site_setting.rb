ActiveAdmin.register SiteSetting do
	menu false
	
	permit_params :maximum_site_tabs, :maximum_site_items
end
