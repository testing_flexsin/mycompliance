ActiveAdmin.register Feature do
	menu false
	
	permit_params :image, :description
	
	filter :description
	filter :created_at, as: :date_range
	filter :updated_at, as: :date_range

	index do 
		selectable_column
		id_column
		column "Image" do |feature|
			image_tag feature.image, class: "image-preview-small" if feature.image.present? && feature.image.file.exists?
		end
		column :description
		column :created_at
		column :updated_at
		actions
	end

	show do
		attributes_table do
			row :id
			row "Image" do |feature|
				image_tag feature.image, class: "image-preview-small" if feature.image.present? && feature.image.file.exists?
			end
			row :description
			row :created_at
			row :updated_at
		end
		active_admin_comments
	end

	form do |f|
		f.inputs do
			f.input :image, as: :file, image_preview: true
			f.input :description
		end
    f.actions
  end
	
	csv do
		column :id
		column "Image" do |feature|
			if feature.image.present? && feature.image.file.exists?
	      a = '=HYPERLINK("link", "image_identifier")'
	      a = a.gsub("link", ENV["DOMAIN"] + feature.image.url).gsub("image_identifier", feature.image_identifier) rescue nil
	    end
		end
		column :description
		column :created_at
		column :updated_at
  end
end
