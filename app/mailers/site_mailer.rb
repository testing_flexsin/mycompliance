class SiteMailer < ApplicationMailer
  def document_invitation(site_item_invitation, site_item, site, user, token = nil)
		@site_item_invitation = site_item_invitation
		@site_item = site_item
		@site = site
		@user = user
		@token = token
		@user_notification_template = user.user_notification_template
		mail(to: site_item_invitation.upload_access_email, subject: "Invitation Letter")
	end
	
	def invitation_confirmation(site_item_invitation, site_item, site, user)
		@site_item_invitation = site_item_invitation
		@site_item = site_item
		@site = site
		@user = user
		mail(to: user.email, subject: "Invitation Letter Sent")
	end
	
	def new_document_upload(site_item_document, site_item, site, user)
		@site_item_document = site_item_document
		@site_item = site_item
		@site = site
		@user = user
		mail(to: site_item_document.contractor_email, subject: "Document uploaded on your behalf")
	end
	
	def approve_document(site_item_document, user)
		@site_item_document = site_item_document
		@site_item = site_item_document.site_item
		@site = site_item_document.site
		@user = user
		@user_notification_template = user.user_notification_template
		mail(to: site_item_document.contractor_email, subject: "Document set as approved")
	end
	
	def pending_document(site_item_document, user)
		@site_item_document = site_item_document
		@site_item = site_item_document.site_item
		@site = site_item_document.site
		@user = user
		@user_notification_template = user.user_notification_template
		mail(to: site_item_document.contractor_email, subject: "Document set as pending")
	end
	
	def reject_document(site_item_document, user)
		@site_item_document = site_item_document
		@site_item = site_item_document.site_item
		@site = site_item_document.site
		@user = user
		@user_notification_template = user.user_notification_template
		mail(to: site_item_document.contractor_email, subject: "Document rejected")
	end
end
