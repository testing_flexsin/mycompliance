class UserMailer < ApplicationMailer
  def contact_us(contact)
		@contact = contact
		static_page = StaticPage.last
		if static_page.present? && static_page.admin_email.present?
			mail(to: static_page.admin_email, subject: "Contact Us")
		else
		end
		mail(to: ENV["ADMIN_MAIL"], subject: "Contact Us")
	end
	
	def password_updated(user, password)
		@user = user
		@password = password
		@email_template = EmailTemplate.first
		mail(to: user.email, subject: "MyCompliance account updated")
	end
	
	def account_created(user, password)
		@user = user
		@password = password
		@email_template = EmailTemplate.first
		mail(to: user.email, subject: "MyCompliance account created")
	end
	
	def manager_created(user, manager, password)
		@user = user
		@manager = manager
		@password = password
		@email_template = EmailTemplate.first
		mail(to: @user.email, subject: "Manager Account Created")
	end
	
	def manager_account(manager, password)
		@manager = manager
		@password = password
		@email_template = EmailTemplate.first
		mail(to: @manager.email, subject: "MyCompliance Manager Account Created")
	end
	
	def manager_updated(manager, password, password_updated)
		@manager = manager
		@password = password
		@password_updated = password_updated
		@email_template = EmailTemplate.first
		mail(to: @manager.email, subject: "MyCompliance Manager Account Updated")
	end
end
