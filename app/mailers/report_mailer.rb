class ReportMailer < ApplicationMailer
  def email_report(report)
		@report = report
		mail(to: report.to_email, cc: report.cc_email, subject: "MyCompliance Report")
	end
end
