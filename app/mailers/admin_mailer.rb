class AdminMailer < ApplicationMailer
  def confirmation_instructions(admin_user)
		@email_template = EmailTemplate.first
		@admin_user = admin_user
		mail(to: admin_user.email, subject: "Confirmation instructions")
	end
	
	def password_updated(admin_user, password)
		@email_template = EmailTemplate.first
		@admin_user = admin_user
		@password = password
		mail(to: admin_user.email, subject: "MyCompliance account updated")
	end
	
	def account_created(admin_user, password)
		@email_template = EmailTemplate.first
		@admin_user = admin_user
		@password = password
		mail(to: admin_user.email, subject: "MyCompliance account created")
	end

	def send_inquiry_email(inquiry)
		@email_template = EmailTemplate.first
		@inquiry = inquiry
		mail(to: @email_template.admin_email, subject: "Inquiry Form submitted created")
	end

	def send_newsletter_email(newsletter)
		@email_template = EmailTemplate.first
		@newsletter = newsletter
		mail(to: @email_template.admin_email, subject: "NewsLetter Form submitted created")
	end

	def contact_us(contact)
		@email_template = EmailTemplate.first
		@contact = contact
		mail(to: @email_template.admin_email, subject: "Contact Form submitted created")
	end
end
