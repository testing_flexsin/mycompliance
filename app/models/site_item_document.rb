class SiteItemDocument < ActiveRecord::Base
  ## Relations
  belongs_to :user
  belongs_to :site
  belongs_to :site_item
  has_many :upload_notifications, dependent: :destroy
  
	## Validations
	validates :user_id, :site_id, :site_item_id, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
	# validates :file, presence: true
  
  mount_uploader :file, DocumentUploader
end
