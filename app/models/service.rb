class Service < ActiveRecord::Base
	## Friendly ID
	extend FriendlyId
	friendly_id :name

	 ## Validations
  validates :name, :description, presence: true

  mount_uploader :image, ImageUploader

 ## Override Friendly ID Method for generating slug
	def normalize_friendly_id(name)
    slug = name.gsub("-", "_").gsub(" ", "_").gsub(".", "_")
		news_count = Service.where("slug LIKE ?", "#{slug}%").count
		if news_count > 0
	  	super.gsub(".", "_").gsub("-", "_") + news_count.to_s
	  else
	  	super.gsub(".", "_").gsub("-", "_")
	  end
	end
end
