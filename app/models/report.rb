class Report < ActiveRecord::Base
	## Relations
	belongs_to :user
	has_many :report_documents, dependent: :destroy
end
