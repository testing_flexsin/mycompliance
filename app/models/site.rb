class Site < ActiveRecord::Base
	## Soft Delete
	# acts_as_paranoid
	
	## Friendly ID
	extend FriendlyId
	friendly_id :name

  ## Relations
  belongs_to :user
	has_many :site_tabs, dependent: :destroy
	has_many :site_items, dependent: :destroy
	has_many :site_item_questions, dependent: :destroy
	has_many :site_item_documents, dependent: :destroy
	has_many :site_item_invitations, dependent: :destroy
	
	accepts_nested_attributes_for :site_tabs, reject_if: lambda { |s| s[:name].blank? }, allow_destroy: true
	
	## Validations
	validates :user_id, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
	validates :name, presence: true
  
  ## Override Friendly ID Method for generating slug
	def normalize_friendly_id(name)
    slug = name.gsub(".", "_").gsub(" ", "_").downcase
		site_count = Site.where("slug LIKE ?", "#{slug}%").count
		if site_count > 0
	  	super.gsub("-", "_").gsub(".", "_") + site_count.to_s
	  else
	  	super.gsub("-", "_").gsub(".", "_")
	  end
	end
	
	## Class Methods
	def self.get_site_with_relation(site_slug)
		self.eager_load(:site_tabs).friendly.find(site_slug)
	end
	
	def self.get_site(site_slug)
		self.friendly.find(site_slug)
	end
	
	def self.current_sites(user_id, page=nil, per_page=nil, order_by="id", order_dir="ASC")
		if page.present? && per_page.present?
			self.eager_load(:site_tabs).where(user_id: user_id, active: true).paginate(page: page, per_page: per_page).order("sites.#{order_by} #{order_dir}")
		else
			self.eager_load(:site_tabs).where(user_id: user_id, active: true).order("sites.#{order_by} #{order_dir}")
		end
	end
	
	def self.site_tabs(tab_name)
		self.where(site_tabs: {name: tab_name})
	end
	
	def self.templates
		self.where(template: true).order("id ASC")
	end
end
