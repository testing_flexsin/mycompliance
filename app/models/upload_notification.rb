class UploadNotification < ActiveRecord::Base
  belongs_to :user
  belongs_to :site_item_document
  
  # Class Methods
  def self.read
    self.where(is_read: true).order("id DESC")
  end
  
  def self.unread
    self.where(is_read: false).order("id DESC")
  end
end
