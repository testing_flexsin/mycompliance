class ContactUs < ActiveRecord::Base
	validates :contact_number, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
	validates :name, :email, :message, presence: true  
end
