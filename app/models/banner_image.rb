class BannerImage < ActiveRecord::Base
  ## Validations
  validates :image, presence: true

  mount_uploader :image, ImageUploader
end
