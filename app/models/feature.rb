class Feature < ActiveRecord::Base
  ## Validations
  validates :description, presence: true
  scope :all_except, ->(feature) { where.not(id: feature) }

  mount_uploader :image, ImageUploader
end
