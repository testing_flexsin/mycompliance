class UserPreferredEmail < ActiveRecord::Base
  ## Relations
  before_save :full_name
  belongs_to :user
  
	## Validations
	validates :user_id, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
	validates :email, presence: true                 

	def full_name
		first_name = self.first_name.present? ? self.first_name : ""
		last_name = self.last_name.present? ? self.last_name : ""
		self.full_name = first_name + " " + last_name
	end

	def self.import(file, user_id)
    if file.present?
      CSV.foreach(file.path, headers: true) do |row|
      	row["user_id"] = user_id
      	UserPreferredEmail.create! row.to_hash
      end
    end
  end
end
