class UploadFileType < ActiveRecord::Base
  ## Validations
  validates :upload_type, presence: true
end
