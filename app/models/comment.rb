class Comment < ActiveRecord::Base
  ## Relations
  belongs_to :news
  
	## Validations
	validates :news_id, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
	validates :email, :message, presence: true
	
	# Comment support hierarchies
	has_closure_tree order: 'created_at ASC'
	
	def to_digraph_label
	  message
	end
end
