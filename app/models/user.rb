class User < ActiveRecord::Base
	## Soft Delete
	acts_as_paranoid

	## Friendly ID
	extend FriendlyId
	friendly_id :user_name
  
  rolify
  # Include default devise modules. Others available are:
  # :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable, :confirmable, authentication_keys: [:login]
  
  ## Relations
  has_one :company_information, dependent: :destroy
  has_many :managers, class_name: "User", foreign_key: "company_id"
  has_many :sites, dependent: :destroy
  has_many :site_tabs, dependent: :destroy
	has_many :site_items, dependent: :destroy
	has_many :site_item_templates, -> { where(template: true) }, class_name: "SiteItem"
	has_many :site_item_documents, dependent: :destroy
  has_many :site_item_invitations, dependent: :destroy
  has_one :user_email_notification, dependent: :destroy
  has_one :user_notification_template, dependent: :destroy
	has_many :user_preferred_emails, dependent: :destroy
  has_many :transaction_details, dependent: :destroy
  has_many :reports, dependent: :destroy
  has_many :upload_notifications, dependent: :destroy
  belongs_to :plan
  
  accepts_nested_attributes_for :company_information, reject_if: lambda { |c| c[:name].blank? }, allow_destroy: true
  
  ## Validations
  validates :first_name, :last_name, :user_name, presence: true
  # Only allow letter, number, underscore and punctuation.
  validates :user_name, uniqueness: { case_sensitive: false }, format: { with: /^[a-zA-Z0-9_\.]*$/, multiline: true }, exclusion: { in: %w(super super_admin admin user admin_user user_admin), message: "%{value} is taken." }
  validates :mobile_number, length: { in: 8..15 }, numericality: { only_integer: true }, allow_nil: true
  # validates :password, format: { with: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}$/, message: "must include one number, one uppercase letter and one lowercase letter.", multiline: true}, on: :create
  validates :terms_and_conditions, acceptance: true

  attr_accessor :login, :current_password
  
  mount_uploader :avatar, ImageUploader
  
  auto_strip_attributes :first_name, :last_name, :user_name
  
  ## Callbacks
  before_create :assign_time_zone
  before_save :assign_full_name
  after_create :import_admin_notification_data
  
  ## Override Friendly ID Method for generating slug
	def normalize_friendly_id(user_name)
    slug = user_name.gsub(".", "_")
		user_count = User.where("slug LIKE ?", "#{slug}%").count
		if user_count > 0
	  	super.gsub("-", "_").gsub(".", "_") + user_count.to_s
	  else
	  	super.gsub("-", "_").gsub(".", "_")
	  end
	end
  
  ## Class Methods
  def self.find_first_by_auth_conditions(warden_conditions)
	  conditions = warden_conditions.dup
	  if login = conditions.delete(:login)
	    where(conditions.to_h).where(["lower(user_name) = :value OR lower(email) = :value", { value: login.downcase }]).first
	  else
	    where(conditions.to_h).first
	  end
	end

  def self.user_email(email)
    self.where("lower(email) = :value", { value: email.downcase }).last
  end

  def self.user_username(user_name)
    self.where("lower(user_name) = :value", { value: user_name.downcase }).last
  end
  
  def self.managers_without_company_information
    self.includes(:company_information).with_role(:company).where.not(id: CompanyInformation.pluck(:user_id).uniq).order("id DESC")
  end
  
  # Return fields to invite
  # def self.invite_key_fields
  #   [:full_name, :user_name, :email]
  # end

  ## Instance Methods

  #this method is called by devise to check for "active" state of the model
  def active_for_authentication?
    super and self.active?
  end
  
  def assign_full_name
    first_name = (self.first_name.present? ? self.first_name : "")
    last_name = (self.last_name.present? ? self.last_name : "")
    self.full_name = first_name + " " + last_name
  end

  def import_admin_notification_data
    admin_notification_template = AdminNotificationTemplate.last
    if admin_notification_template.present?
      user_notification_template = self.build_user_notification_template(
        invitee_template: admin_notification_template.invitee_template,
        email_due_soon_template: admin_notification_template.email_due_soon_template,
        email_overdue_template: admin_notification_template.email_overdue_template,
        email_due_soon_invitee_template: admin_notification_template.email_due_soon_invitee_template,
        email_overdue_invitee_template: admin_notification_template.email_overdue_invitee_template,
        email_report_recipients_template: admin_notification_template.email_report_recipients_template,
        document_feedback_approved_template: admin_notification_template.document_feedback_approved_template,
        document_feedback_pending_template: admin_notification_template.document_feedback_pending_template,
        document_feedback_rejected_template: admin_notification_template.document_feedback_rejected_template
      )
      user_notification_template.save
    end
  end
  
  def assign_time_zone
    self.time_zone = "UTC" if self.time_zone.blank?
  end

  def paypal_url(plan)
    values = {
      business: "#{ENV['BUSINESS_EMAILID']}",
      cmd: "_xclick",
      upload: 1,
      return: "#{ENV['APP_HOST']}/paypal_callback",
      invoice: id,
      amount: plan.price.to_f,
      quantity: '1',
      custom: plan.id, # Plan ID or you can use item_number
      notify_url: "#{ENV['APP_HOST']}"
    }
    "#{ENV['PAYPAL_HOST']}/cgi-bin/webscr?" + values.to_query
  end
end
