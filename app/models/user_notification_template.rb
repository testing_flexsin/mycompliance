class UserNotificationTemplate < ActiveRecord::Base
  ## Relations
  belongs_to :user
  
	## Validations
	validates :user_id, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
end

