class AdminUser < ActiveRecord::Base
  include DeviseInvitable::Inviter
  
  ## Friendly ID
	extend FriendlyId
	friendly_id :email
  
  rolify
  
  # Include default devise modules. Others available are:
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable, :confirmable, authentication_keys: [:email]
  
  attr_accessor :sub_admin, :login
  
  ## Override Friendly ID Method for generating slug
  def normalize_friendly_id(email)
    slug = email.split("@").first.gsub(".", "_") rescue ""
  	user_count = AdminUser.where("slug LIKE ?", "#{slug}%").count + 1
    slug + user_count.to_s
  end
  
  ## Instance Methods

  #this method is called by devise to check for "active" state of the model
  def active_for_authentication?
    super and self.active?
  end
  
  # Callback initiated after successfully confirming
  # def after_confirmation
  # end
  
  protected
  
  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
	    where(conditions).where(["lower(email) = :value", { value: login.strip.downcase }]).first
	  else
	    where(conditions.to_h).first
	  end
  end
end
