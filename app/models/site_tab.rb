class SiteTab < ActiveRecord::Base
	## Friendly ID
	extend FriendlyId
	friendly_id :name
	
  ## Relations
  belongs_to :user
  belongs_to :site
	has_many :site_items, dependent: :destroy
  
	## Validations
	validates :site_id, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
	validates :name, presence: true
  
  ## Override Friendly ID Method for generating slug
	def normalize_friendly_id(name)
    slug = name.gsub(".", "_").gsub(" ", "_").downcase
		site_tab_count = SiteTab.where("slug LIKE ?", "#{slug}%").count
		if site_tab_count > 0
	  	super.gsub("-", "_").gsub(".", "_") + site_tab_count.to_s
	  else
	  	super.gsub("-", "_").gsub(".", "_")
	  end
	end
end
