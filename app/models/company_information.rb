class CompanyInformation < ActiveRecord::Base
  ## Relations
  belongs_to :user
  
	## Validations
	validates :user_id, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
	validates :name, :city, :state, :country, :zip_code, :organization_type, :number_of_employees, presence: true
  
  mount_uploader :logo, ImageUploader
end
