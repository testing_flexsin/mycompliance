class SiteItemQuestion < ActiveRecord::Base
  ## Relations
  belongs_to :site
  belongs_to :site_item
  
	## Validations
	# validates :site_item_id, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
	validates :question, presence: true
end
