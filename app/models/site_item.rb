class SiteItem < ActiveRecord::Base
	## Friendly ID
	extend FriendlyId
	friendly_id :title
	
  ## Relations
  belongs_to :user
  belongs_to :site
  belongs_to :site_tab
  has_many :site_item_questions, dependent: :destroy
  has_many :site_item_documents, dependent: :destroy
	has_many :site_item_invitations, dependent: :destroy
	
	accepts_nested_attributes_for :site_item_questions, reject_if: lambda { |q| q[:question].blank? }, allow_destroy: true
  
	## Validations
	validates :user_id, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
	validates :title, presence: true
	
  ## Override Friendly ID Method for generating slug
	def normalize_friendly_id(title)
    slug = title.gsub(".", "_").gsub(" ", "_").downcase
		site_item_count = SiteItem.where("slug LIKE ?", "#{slug}%").count
		if site_item_count > 0
	  	super.gsub("-", "_").gsub(".", "_") + site_item_count.to_s
	  else
	  	super.gsub("-", "_").gsub(".", "_")
	  end
	end
	
  ## Class Methods
	def self.templates(user_id)
		self.where(user_id: user_id, template: true).order("site_items.id ASC")
	end
end
