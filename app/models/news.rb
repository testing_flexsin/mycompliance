class News < ActiveRecord::Base
	## Friendly ID
	extend FriendlyId
	friendly_id :title

  ## Relations
  has_many :comments, dependent: :destroy
  
	## Validations
	validates :title, :description, presence: true
  
  mount_uploader :image, ImageUploader

  ## Override Friendly ID Method for generating slug
	def normalize_friendly_id(title)
    slug = title.gsub("-", "_").gsub(" ", "_").gsub(".", "_")
		news_count = News.where("slug LIKE ?", "#{slug}%").count
		if news_count > 0
	  	super.gsub(".", "_").gsub("-", "_") + news_count.to_s
	  else
	  	super.gsub(".", "_").gsub("-", "_")
	  end
	end
end
