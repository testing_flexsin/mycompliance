class SiteSetting < ActiveRecord::Base
	## Validations
	validates :maximum_site_tabs, :maximum_site_items, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
end
