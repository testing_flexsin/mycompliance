class Testimonial < ActiveRecord::Base
  ## Validations
  validates :name, :message, presence: true

  mount_uploader :image, ImageUploader
end
