class NumberOfEmployee < ActiveRecord::Base
  ## Validations
  validates :employees, presence: true
end
