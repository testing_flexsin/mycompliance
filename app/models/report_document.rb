class ReportDocument < ActiveRecord::Base
	## Relations
	belongs_to :report

  mount_uploader :document, DocumentUploader
end
