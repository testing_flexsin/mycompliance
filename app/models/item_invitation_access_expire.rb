class ItemInvitationAccessExpire < ActiveRecord::Base
  ## Validations
  validates :expires, presence: true
end
