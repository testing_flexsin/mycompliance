class Plan < ActiveRecord::Base
	## Relations
  has_many :users
  has_many :transaction_details

  validates :name,:price,:time_duration_postfix, :time_duration, presence: true
end
