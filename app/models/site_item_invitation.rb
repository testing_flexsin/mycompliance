class SiteItemInvitation < ActiveRecord::Base
  ## Relations
  belongs_to :user
  belongs_to :site
  belongs_to :site_item
  
	## Validations
	validates :user_id, :site_id, :site_item_id, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
	validates :upload_access_email, presence: true
  
  ## Class Methods
	def self.get_site_item_invitations(user)
		self.where(pause: false, upload_access_email: user.email).order("id ASC")
	end
end
