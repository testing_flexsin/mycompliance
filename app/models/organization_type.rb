class OrganizationType < ActiveRecord::Base
  ## Validations
  validates :org_type, presence: true
end
