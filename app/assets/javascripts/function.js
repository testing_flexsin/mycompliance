$(document).ready(function(){
/**homeSlider**/
$('.homeSlider').owlCarousel({
		loop:true,
		margin:0,
		nav:true,
		items:1,
		smartSpeed:1500,
		autoplay:true,
})
/**Signup Toggle**/
$(".signmeupBtn").click(function(){
		$(".signupMain").slideToggle(500);
		$(this).toggleClass('open');
});
/**testmonialSlider**/
var test = $('.testmonialSlider');
test.owlCarousel({
		loop:true,
		margin:0,
		nav:true,
		items:1,
		smartSpeed:1500,
		autoplay:true,
		animateIn: 'fadeIn',
		animateOut: 'fadeOut'
})
$('.LeftArrow').click(function() {
    test.trigger('next.owl.carousel');
})
$('.RightArrow').click(function() {
    test.trigger('prev.owl.carousel', [300]);
})

/**WOW Animation**/
wow = new WOW(
  {
  boxClass:     'wow',      // default
  animateClass: 'animated', // default
  offset:       0,          // default
  mobile:       true,       // default
  live:         true        // default
	}
)
wow.init();

$(".mobnav").click(function(){
	$(".menuBar").toggleClass("opens");
});

/*--------------Login popup--------------*/
$(".LoginPoPUp").click(function(){
	$("#signuppopup").fadeOut(400);
	$("body").removeClass("hiddens");
	$("#loginpopup").fadeIn(400);
	$("body").addClass("hiddens");
});
$(".closebtn").click(function(){
	$("#loginpopup").fadeOut(400);
	$("body").removeClass("hiddens");
});

/*--------------Signup popup--------------*/
$(".signUpBtn").click(function(){
	$("#loginpopup").fadeOut(400);
	$("body").removeClass("hiddens");
	$("#signuppopup").fadeIn(400);
	$("body").addClass("hiddens");
});
$(".closebtn2").click(function(){
	$("#signuppopup").fadeOut(400);
	$("body").removeClass("hiddens");
});
 
 /**MouseUp**/
$(document).mouseup(function (e)
  {
  var container = $(".mobnav");
  var container1 = $(".menuBar");
  var container2 = $(".menuBar *");
  if (!container.is(e.target)  && !container1.is(e.target)    && !container2.is(e.target)  && container.has(e.target).length === 0) 
  {    
 
   $(".menuBar").removeClass("opens");
  } 
});

$(document).mouseup(function (e) {
	var container = $(".dropmenuSetting");
	var container1 = $(".dropDown");
	var container2 = $(".dropDown *");
	if (!container.is(e.target)  && !container1.is(e.target)    && !container2.is(e.target)  && container.has(e.target).length === 0) {
		$(".dropDown").slideUp(200);
		$(".dropmenuSetting").removeClass("active");	
	} 
});

$(".dropmenuSetting").click(function(){
	if(!$(this).hasClass("active")){
		$(".dropDown").slideUp(200);
		$(".dropmenuSetting").removeClass("active");	
		$(this).addClass("active");
		$(this).next(".dropDown").slideDown(200);	
	} else {
		$(this).removeClass("active");
		$(this).next(".dropDown").slideUp(200);
	}
});


	$(".ftrHead").click(function(){
	if($(window).width() < 767){
	$(".ftrCon").slideUp(300);            
		if($(this).parent(".footerBox").hasClass("active")){
			$(".footerBox").removeClass("active");
				$(this).next(".ftrCon").slideUp(300);
			}
			else{
			$(".footerBox").removeClass("active");
				$(this).parent(".footerBox").addClass("active");
				$(this).next(".ftrCon").slideDown(300);
			 }
	}
	});
	
	
	
	
	/*--------tabs------*/
	$(".tabList li a").on("click" , function(e){
		e.preventDefault();
		var weddInd = $(this).attr('href');
		$(".tabsContent").hide(0);
		$(weddInd).show(0);
		$(".tabList li a").removeClass('active');
		$(this).addClass('active');
	});
		

/*--------------add More slider--------------*/ 

$(".documentLink").click(function(){
	$("#documentPop").fadeIn(300);
	$("body").addClass("hiddens");
 });
$(".monMenu").click(function(){
	$(".mobMenuBg").fadeIn(300);
	$("body").addClass("hiddens");
 });
	$(".closePop").click(function(){								 
	$(".popOverlay").fadeOut(300);
	$("body").removeClass("hiddens");
 });	

$(".mobMenuBg").click(function(){
	$(".mobMenuBg").fadeOut(300);										 
	$("body").removeClass("hiddens");
 });	
			
/*all pop js*/

$("#ItemTemp").click(function(){
	$("#itemTemplatePop").fadeIn(300);
	$("body").addClass("hiddens");
 });

$("#myRep").click(function(){
	$("#myReport").fadeIn(300);
	$("body").addClass("hiddens");
 });

$("#muuset").click(function(){
	$("#myUserSetting").fadeIn(300);
	$("body").addClass("hiddens");
 });

$("#muUserNotti").click(function(){
	$("#myUserNoti").fadeIn(300);
	$("body").addClass("hiddens");
 });

$("#prefList").click(function(){
	$("#PreferredEmailList").fadeIn(300);
	$("body").addClass("hiddens");
 });

$("#invit").click(function(){
	$("#sendInvitation").fadeIn(300);
	$("body").addClass("hiddens");
 });

$("#temEd").click(function(){
	$("#temEdit").fadeIn(300);
	$("body").addClass("hiddens");
 });

}); 

(function($){
			$(window).on("load",function(){
				$(".scrollbox").mCustomScrollbar({
					axis:"x",
					theme:"light-3",
					advanced:{autoExpandHorizontalScroll:true}
				});
				if($(window).width() > 767){
				$(".scrollbarCustom").mCustomScrollbar({
					axis:"Y",
					theme:"light-2",
					
				});
				}
			});
		})(jQuery);
