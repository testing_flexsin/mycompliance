//= require active_admin/base
//= require active_admin/select2
// require ckeditor/init
// require_tree .././ckeditor

$(document).ready(function() {
  $('#q_created_at_gteq_date').datepicker({
	  onSelect: function(dateText, inst){
	    $('#q_created_at_lteq_date').datepicker('option', 'minDate', new Date(dateText));
	  },
	});

	$('#q_created_at_lteq_date').datepicker({
	  onSelect: function(dateText, inst){
	    $('#q_created_at_gteq_date').datepicker('option', 'maxDate', new Date(dateText));
	  }
	});
	
  $('#q_updated_at_gteq_date').datepicker({
	  onSelect: function(dateText, inst){
	    $('#q_updated_at_lteq_date').datepicker('option', 'minDate', new Date(dateText));
	  },
	});

	$('#q_updated_at_lteq_date').datepicker({
	  onSelect: function(dateText, inst){
	    $('#q_updated_at_gteq_date').datepicker('option', 'maxDate', new Date(dateText));
	  }
	});
	
  $('#q_deleted_at_gteq_date').datepicker({
	  onSelect: function(dateText, inst){
	    $('#q_deleted_at_lteq_date').datepicker('option', 'minDate', new Date(dateText));
	  },
	});

	$('#q_deleted_at_lteq_date').datepicker({
	  onSelect: function(dateText, inst){
	    $('#q_deleted_at_gteq_date').datepicker('option', 'maxDate', new Date(dateText));
	  }
	});
	
  $('#q_access_expires_date_gteq_date').datepicker({
	  onSelect: function(dateText, inst){
	    $('#q_access_expires_date_lteq_date').datepicker('option', 'minDate', new Date(dateText));
	  },
	});

	$('#q_access_expires_date_lteq_date').datepicker({
	  onSelect: function(dateText, inst){
	    $('#q_access_expires_date_gteq_date').datepicker('option', 'maxDate', new Date(dateText));
	  }
	});
});