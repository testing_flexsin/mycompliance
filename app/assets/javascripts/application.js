//= require jquery.min
//= require jquery_ujs
//= require jquery.remotipart
//= require nested_form
//= require jquery.validate
//= require additional-methods
//= require validations
//= require owl.carousel.min
//= require wow
//= require jquery.mCustomScrollbar.concat.min
//= require moment.min
//= require bootstrap-datetimepicker.min
//= require jquery.inputmask
//= require jquery.inputmask.date.extensions
//= require jquery.inputmask.numeric.extensions
//= require jquery.inputmask.phone.extensions
//= require jquery.inputmask.regex.extensions
//= require jquery.inputmask.extensions
//= require select2.full
//= require private_pub
// require function


$(document).ajaxStart(function() {
  $(".ajax-loading").show();
});

$(document).ajaxStop(function() {	
	addClassActive();
  $(".ajax-loading").hide();
});

function addClassActive() {
	var page_url = window.location.pathname;
	$(".menuBar ul li").removeClass("active");
	$('a[href="'+page_url+'"]').parents(".menuBar ul li").addClass("active");
}

$(window).resize(function(){
	set_footer();
});

function set_footer() {
	// ($("body").height() > $(window).height() ? $("footer").css({"position":"static"}) : $("footer").css({"position":"absolute", "bottom":0}));
}

$(function(){
	addClassActive();
	set_footer();
});

$(document).on('page:change', function() {
	if (window._gaq != null) {
		return _gaq.push(['_trackPageview']);
 	} else if (window.pageTracker != null) {
		return pageTracker._trackPageview();
	}
});
