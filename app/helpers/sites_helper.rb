module SitesHelper
  def site_files_received(site)
    SiteItemDocument.where(site_id: site.id, user_id: current_user.id).order("id DESC")
  end
end
