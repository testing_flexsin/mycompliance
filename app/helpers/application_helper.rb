module ApplicationHelper
  def remote_request(type, path, params={}, target_tag_id)
	  "$.#{type}('#{path}',
      {#{params.collect { |p| "#{p[0]}: #{p[1]}" }.join(", ")}},
      function(data) {$('##{target_tag_id}').html(data);}
	  );"
	end

  def bootstrap_class_for flash_type
    { success: "alert-success", error: "alert-danger", alert: "alert-danger", notice: "alert-info" }[flash_type.to_sym]
  end

  def flash_messages(opts = {})
    flash.each do |msg_type, message|
      concat(content_tag(:div, message, class: "alert #{bootstrap_class_for(msg_type)} fade in margin-bottom-10") do 
        concat content_tag(:a, 'x', class: "close position-inherit", data: { dismiss: 'alert' })
        concat message 
      end)
    end
    nil
  end

  def model_error_messages(model_resource)
    return '' if model_resource.errors.empty?

    messages = model_resource.errors.full_messages.map { |msg| content_tag(:li, msg) }.join
    sentence = I18n.t('errors.messages.not_saved',
                      count: model_resource.errors.count,
                      resource: model_resource.class.model_name.human.downcase)

    html = <<-HTML
    <div class="alert alert-danger fade in margin-bottom-10">
      <a class="close position-inherit" data-dismiss="alert">x</a>
      <ul>#{messages}</ul>
    </div>
    HTML

    html.html_safe
  end

  def value_quote_ref(value = 0)
    if value.to_s.length < 6
      "%06d" % value
    else
      value.to_s
    end
  end

  def value_with_precision(value = 0.0)
    number_with_precision(value, precision: 2, delimiter: ',')
  end

  def value_with_delimiter(value = 0.0)
    number_with_precision(value, precision: 0, delimiter: ',')
  end

  def comments_tree_for(comments)
    comments.map do |comment, nested_comments|
      unless comment.new_record?
        render(partial: "home/comment", locals: {comment: comment}) + ((nested_comments.present? && nested_comments.size > 0) ? content_tag(:div, comments_tree_for(nested_comments), class: "discussionBoxSpace") : nil)
      end
    end.join.html_safe
  end
  
  def upload_document_file_types
    @upload_file_types ||= UploadFileType.pluck(:upload_type).uniq
  end
  
  def sites_per_page_class(count)
    current_user.sites_per_page == count ? "active" : ""
  end
end
