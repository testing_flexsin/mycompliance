module WillPaginateHelper
  class WillPaginateJSLinkRenderer < WillPaginate::ActionView::LinkRenderer
    def prepare(collection, options, template)
      options[:params] ||= {}
      options[:params]['_'] = nil
      super(collection, options, template)
    end

    protected
    def link(text, target, attributes = {})
      if target.is_a? Fixnum
        attributes[:rel] = rel_value(target)
        target = url(target)
      end

      @template.link_to(target, attributes.merge(remote: true)) do
        text.to_s.html_safe
      end
    end

    def page_number(page)
      unless page == current_page
        link(page, page, rel: rel_value(page))
      else
        link(page, "javascript:;", class: "active")
      end
    end

    def previous_or_next_page(page, text, classname)
      if page
        if text == "Next &#8594;"
          link('Next'.html_safe, page, aria: {label: "Next"})
        elsif text == "&#8592; Previous"
          link('Prev'.html_safe, page, aria: {label: "Previous"})
        end
      else
        if text == "Next &#8594;"
          link("Next", "javascript:;")
        elsif text == "&#8592; Previous"
          link("Prev", "javascript:;")
        end
      end
    end

    def html_container(html)
      tag(:span, html, container_attributes)
    end
  end

  def js_will_paginate(collection, options = {})
    will_paginate(collection, options.merge(renderer: WillPaginateHelper::WillPaginateJSLinkRenderer))
  end
end