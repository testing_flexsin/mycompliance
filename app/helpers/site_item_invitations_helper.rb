module SiteItemInvitationsHelper
  def item_invitation_access_expires
    @item_invitation_access_expires ||= ItemInvitationAccessExpire.order("id ASC")
  end
end
