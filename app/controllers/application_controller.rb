class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  rescue_from CanCan::AccessDenied, with: :access_denied

  ## Filters
  skip_before_action :verify_authenticity_token, if: Proc.new { |c| c.request.format == 'application/json' }
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :store_location, :get_time_zone, :get_new_user, :get_newsletter, :get_static_page, :check_profile, :recent_post
  helper_method :current_role, :user_permission
  
  def current_role
    if user_signed_in?
      @current_role ||= current_user.roles.pluck(:name)
    end
  end

  def recent_post
    @popular_news = News.joins(:comments).select('news.*, count(comments.id) as comment_count').group("news.id").order("comment_count DESC").first(3)
    if @popular_news.size <= 2
      @latest_popular_news = News.where.not(id: @popular_news.select{|n| n.id}.uniq).order("created_at DESC").first(3-@popular_news.size)
    end
  end

  def user_permission(permission)
    if ((current_role.include? "manager") && current_user.permissions.present? && (current_user.permissions.include? permission)) || (current_role.include? "company")
      true
    else
      false
    end
  end
  
  def store_location
    return unless request.get?
    if (request.path != "/" &&
        request.path != "/users/sign_in" &&
        request.path != "/users/sign_up" &&
        request.path != "/users/password/new" &&
        request.path != "/users/password/edit" &&
        request.path != "/users/confirmation/new" &&
        request.path != "/users/sign_out" &&
        request.path != "/users/retrieve_password/new" &&
        !(request.path.include? "/admin") &&
        !request.xhr?)
      session[:previous_url] = request.fullpath
    end
  end
  
  def get_time_zone
    Time.zone = current_user.time_zone if user_signed_in? && current_user.time_zone.present?
  end
  
  def get_new_user
    @new_user = User.new if !user_signed_in?
  end

  def get_newsletter
    @newsletter = Newsletter.new
  end

  def get_static_page
    @static_page = StaticPage.last
  end

  def check_profile
    # if current_user.present? && !current_user.first_name.present?
      # redirect_to users_update_path
    # end
  end
  
  def after_sign_in_path_for(resource)
    if resource.class.name == "User"
      if current_user.sign_in_count == 1
        users_update_path
      else
        session[:previous_url] || root_path
      end
    else
      admin_dashboard_path
    end
  end

  def after_sign_up_path_for(resource)
    resource.class.name == "User" ? new_user_session_path : admin_dashboard_path
  end

  def after_sign_out_path_for(resource)
    session[:previous_url] = nil
    root_path
  end
  
  def access_denied(exception)
    redirect_to root_path, alert: exception.message
  end
  
  def access_denied_admin(exception)
    redirect_to admin_dashboard_path, alert: exception.message
  end
  
  protected
    def record_not_found
      redirect_to root_path
    end

    def configure_permitted_parameters
      devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:first_name, :last_name, :full_name, :user_name, :email, :password, :password_confirmation) }
      devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :user_name, :email, :password, :remember_me) }
      devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:first_name, :last_name, :full_name, :user_name, :email, :password, :password_confirmation, :current_password) }
      # devise_parameter_sanitizer.for(:invite).concat [:first_name, :last_name, :full_name, :user_name]
      devise_parameter_sanitizer.for(:invite) { |u| u.permit(:first_name, :last_name, :full_name, :user_name, :email) }
      devise_parameter_sanitizer.for(:accept_invitation).concat [:first_name, :last_name, :full_name, :user_name]
      devise_parameter_sanitizer.for(:accept_invitation) { |u| u.permit(:first_name, :last_name, :full_name, :user_name, :password, :password_confirmation, :invitation_token) }
    end
end
