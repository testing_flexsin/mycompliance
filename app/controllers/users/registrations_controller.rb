class Users::RegistrationsController < Devise::RegistrationsController
	respond_to :html, :js
	after_action :add_company_role, only: [:create]
	
  protected
  	def add_company_role
  		resource.add_role :company
  	end

		def update_resource(resource, params)
			if params[:password].present?
				result = resource.update_with_password(params)
				sign_in resource, bypass: true
			else
				result = resource.update_without_password(params)
			end
		end

	  def after_inactive_sign_up_path_for(resource)
	  	plan= Plan.find(params[:user][:plan_id]) rescue ""
	  	if plan.present?
				resource.paypal_url(plan)
			else
				root_path
			end
	  end

	  def after_update_path_for(resource)
      my_settings_path
    end
end