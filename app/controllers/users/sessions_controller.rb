class Users::SessionsController < Devise::SessionsController
  skip_before_action :check_profile
  
  respond_to :html, :js
end