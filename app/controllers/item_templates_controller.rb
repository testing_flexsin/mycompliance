class ItemTemplatesController < ApplicationController
  ## Filters
  before_action :authenticate_user!
  before_action :get_item, only: [:update, :destroy_show, :destroy, :add_question_template]
  skip_before_action :get_static_page, only: [:destroy_show]
  
  def index
     if user_permission("manage_item_template")
      @site_item = current_user.site_items.new
      @site_item.site_item_questions.new
      @item_templates = SiteItem.templates(current_user.id).eager_load(:site_item_questions)
    else
      redirect_to root_path  
    end
  end
  
  def create
    @site_item = current_user.site_items.new(site_item_params)
    @site_item.template = true
    if @site_item.save
      @site_items_size = SiteItem.templates(current_user.id).count rescue 1
      @new_item = current_user.site_items.new
      @new_item.site_item_questions.new
      @saved = true
    else
      @saved = false
    end
  end
  
  def update
    if @item.update(site_item_params)
      @site_items_size = SiteItem.templates(current_user.id).count rescue 1
      @saved = true
    else
      @saved = false
    end
  end

  def destroy_show
  end

  def destroy
    @item_id = @item.id
    @destroy = @item.destroy
  end
  
  private
    def get_item
      @item = SiteItem.find(params[:id]) rescue ""
      unless @item.present?
        redirect_to root_path
      end
    end
  
    def site_item_params
      params.require(:site_item).permit(:user_id, :title, :frequency, :required_for_site_compliance, :due_date, :due_soon, site_item_questions_attributes: [:id, :question, :required_for_compliant, :_destroy])
    end
end
