class ReportsController < ApplicationController
	require "report_pdf"
	## Filters
	before_action :authenticate_user!
	skip_before_action :get_static_page
	before_action :get_report, only: [:show, :destroy]

	def index
		get_reports
	end
	
	def new
		@report = current_user.reports.new
		@sites = Site.current_sites(current_user.id)
		@site_tabs = current_user.site_tabs.order("id ASC").pluck(:name).uniq
	end

	def create
		sites = report_params[:sites].to_i rescue ""
		if sites > 0
			params[:report][:site_id] = sites
			params[:report][:sites] = ""
		end
		if report_params[:report_type] != "let_me_choose_report"
			params[:report][:tabs] = [""]
			params[:report][:items] = [""]
		end
		binding.pry
		if report_params[:send_report].include? "email_report"
			params[:report][:to_email] = report_params[:to_email].reject(&:empty?) if report_params[:to_email].present?
	    params[:report][:cc_email] = report_params[:cc_email].reject(&:empty?) if report_params[:cc_email].present?
		else
			params[:report][:to_email] = [""]
	    params[:report][:cc_email] = [""]
		end
		@report = current_user.reports.new(report_params)
		if @report.save
			if @report.site_id.present?
				@sites = Site.eager_load(:site_tabs).where(id: @report.site_id)
			else
				@sites = []
				case @report.sites
				when "all_sites"
					get_all_sites
				when "compliant_sites"
					@sites = Site.eager_load(:site_tabs).where(user_id: current_user.id, active: true, compliant: true).order("sites.id ASC")
				when "due_soon_sites"
				when "non_compliant_sites"
					@sites = Site.eager_load(:site_tabs).where(user_id: current_user.id, active: true, compliant: false).order("sites.id ASC")
				when "due_non_compliant_sites"
				else
					get_all_sites
				end
			end
			
			if @report.report_format.include? "pdf"
				pdf = ReportPdf.new(@sites)
        # pdf.encrypt_document(
		    #   user_password: 'password', 
		    #   owner_password: "hellopassword",
		    #   permissions: {
				# 		print_document: false,
        #     modify_contents: false,
        #     copy_contents: false,
        #     modify_annotations: false
				# 	}
				# )
				filename = "#{@report.id}-#{Time.now.to_i}.pdf"
				dir = File.dirname("#{Rails.root}/public/uploads/reports/#{filename}")
  			FileUtils.mkdir_p(dir) unless File.directory?(dir)
				pdf.render_file File.join(Rails.root, "public/uploads/reports", filename)
				@report.report_documents.create(document: Pathname.new("#{Rails.root}/public/uploads/reports/#{filename}").open)
				File.delete("#{Rails.root}/public/uploads/reports/#{filename}")
			end
			
			if @report.report_format.include? "excel"
				xlsx_package = Axlsx::Package.new
				wb = xlsx_package.workbook
				wb.styles do |style|
					highlight_cell = style.add_style(
						bg_color: "EFC376",
						border: Axlsx::STYLE_THIN_BORDER,
						alignment: { horizontal: :center },
						num_fmt: 8
					)

					date_cell = style.add_style(format_code: "yyyy-mm-dd", border: Axlsx::STYLE_THIN_BORDER)

				  wb.add_worksheet(name: "Sites") do |sheet|
			     	sheet.add_row ["Name", "Created At", "Updated At"]

			      sheet.merge_cells "A1:A2"
			      sheet.merge_cells "B1:B2"
			      sheet.merge_cells "C1:C2"
			      sheet.merge_cells "A1:B2"
			      sheet.merge_cells "A3:B3"

			      sheet.merge_cells sheet.rows.last.cells[(3..4)]

			     	@sites.each do |site|
			  	 		#sheet.merge_cells "A1:B1"
			     		sheet.add_row [site.name, site.created_at, site.updated_at], style: [nil, highlight_cell, date_cell]
		     			sheet.add_hyperlink location: "'Another Sheet'!B2", ref: sheet.rows.last.cells.first, target: :sheet
				    end
				    sheet.add_row ['Total', "=SUM(B1:B#{@sites.length})"]
				  end
				end
				filename = "#{@report.id}-#{Time.now.to_i}.xlsx"
				dir = File.dirname("#{Rails.root}/public/uploads/reports/#{filename}")
  			FileUtils.mkdir_p(dir) unless File.directory?(dir)
				File.open("#{Rails.root}/public/uploads/reports/#{filename}", 'wb') do |file|
				  file.write(xlsx_package.to_stream.read)
				end
				@report.report_documents.create(document: Pathname.new("#{Rails.root}/public/uploads/reports/#{filename}").open)
				File.delete("#{Rails.root}/public/uploads/reports/#{filename}")
			end
			
			if @report.report_format.include? "doc"
				filename = "#{@report.id}-#{Time.now.to_i}.docx"
				dir = File.dirname("#{Rails.root}/public/uploads/reports/#{filename}")
				FileUtils.mkdir_p(dir) unless File.directory?(dir)
				Caracal::Document.save "public/uploads/reports/" + filename do |docx|
					#-----------------------------------------------------
					# page settings
					#-----------------------------------------------------
					docx.page_numbers true do
					  align :center
					end

					docx.font name: 'Courier New'

					docx.style id: 'AltFont', name: 'altFont', font: 'Palatino'

					#-----------------------------------------------------
					# cover
					#-----------------------------------------------------
					7.times do
					  docx.p
					end

					12.times do
					  docx.p
					end
					docx.p 'MyCompliance Report', style: 'Title', align: :right
					docx.p 'Report', style: 'Subtitle', align: :right
					docx.p
					docx.p
					docx.p Date.today.strftime('%B %d, %Y'), color: '666666', align: :right

					#-----------------------------------------------------
					# introduction
					#-----------------------------------------------------
					docx.page
					docx.h1 'Site(s)'
					docx.hr
					
					@sites.each do |site|
						docx.h2 "Site - #{site.name}", bold: true, underline: true
						if @report.report_type == "compliance_report"
							docx.p 'Compliance Report.'
							docx.p
							site.site_tabs.each do |site_tab|
								docx.p "Tab - #{site_tab.name}.", bold: true
								docx.p
								site_items = site_tab.site_items.eager_load(:site_item_questions).order("site_items.id ASC, site_item_questions.id ASC").where(required_for_site_compliance: true)
								site_items.each do |site_item|
									docx.p "Item - #{site_item.title} - #{site_item.frequency}.", bold: true
									docx.p
									site_item_questions = site_item.site_item_questions
									site_item_questions.each  do |site_item_question|
										docx.p "Question - #{site_item_question.question} - #{site_item_question.required_for_compliant}."
										docx.p
									end
									documents = []
									site_item_documents = site_item.site_item_documents
									if @report.documents.include? "all_current_documents"
										site_item_documents.select{|item_document| !item_document.archive?}.each do |site_item_document|
											documents << site_item_document
										end
									end
									if @report.documents.include? "archived_documents"
										site_item_documents.select{|item_document| item_document.archive?}.each do |site_item_document|
											documents << site_item_document
										end
									end
									documents.each do |document|
										docx.p "Document - #{document.file_identifier}."
										docx.p do
										  link "#{document.file_identifier}", "#{ENV['DOMAIN']}#{document.file.url}"
										end
										docx.p
									end
								end
							end
						elsif @report.report_type == "full_building_report"
							docx.p 'Full Building Report.'
							docx.p
							site.site_tabs.each do |site_tab|
								docx.p "Tab - #{site_tab.name}.", bold: true
								docx.p
								site_items = site_tab.site_items.eager_load(:site_item_questions).order("site_items.id ASC, site_item_questions.id ASC")
								site_items.each do |site_item|
									docx.p "Item - #{site_item.title} - #{site_item.frequency}.", bold: true
									docx.p
									site_item_questions = site_item.site_item_questions
									site_item_questions.each  do |site_item_question|
										docx.p "Question - #{site_item_question.question} - #{site_item_question.required_for_compliant}."
										docx.p
									end
									documents = []
									site_item_documents = site_item.site_item_documents
									if @report.documents.include? "all_current_documents"
										site_item_documents.select{|item_document| !item_document.archive?}.each do |site_item_document|
											documents << site_item_document
										end
									end
									if @report.documents.include? "archived_documents"
										site_item_documents.select{|item_document| item_document.archive?}.each do |site_item_document|
											documents << site_item_document
										end
									end
									documents.each do |document|
										docx.p "Document - #{document.file_identifier}."
										docx.p do
										  link "#{document.file_identifier}", "#{ENV['DOMAIN']}#{document.file.url}"
										end
										docx.p
									end
								end
							end
						elsif @report.report_type == "let_me_choose_report"
							docx.p 'Choose Report.'
							docx.p
							site_tabs = @report.tabs
							if site_tabs.include? "All Tabs"
								site_tabs = site_tabs - ["All Tabs"]
							end
							site.site_tabs.where(name: site_tabs).each do |site_tab|
								docx.p "Tab - #{site_tab.name}.", bold: true
								docx.p
								site_items = []
								site_tab_items = site_tab.site_items.eager_load(:site_item_questions, :site_item_documents).order("site_items.id ASC, site_item_questions.id ASC")
								if @report.items.include? "compliant_items"
									site_tab_items.select{|tab_item| tab_item.compliant?}.each do |item|
										site_items << item
									end
								elsif @report.items.include? "due_soon_items"
								elsif @report.items.include? "non_compliant_items"
									site_tab_items.select{|tab_item| !tab_item.compliant?}.each do |item|
										site_items << item
									end
								end
								site_items.each do |site_item|
									docx.p "Item #{site_item.id} - #{site_item.title} - #{site_item.frequency}.", bold: true
									docx.p
									site_item_questions = site_item.site_item_questions
									site_item_questions.each  do |site_item_question|
										docx.p "Question - #{site_item_question.question} - #{site_item_question.required_for_compliant}."
										docx.p
									end
									documents = []
									site_item_documents = site_item.site_item_documents
									if @report.documents.include? "all_current_documents"
										site_item_documents.select{|item_document| !item_document.archive?}.each do |site_item_document|
											documents << site_item_document
										end
									end
									if @report.documents.include? "archived_documents"
										site_item_documents.select{|item_document| item_document.archive?}.each do |site_item_document|
											documents << site_item_document
										end
									end
									documents.each do |document|
										docx.p "Document - #{document.file_identifier}."
										docx.p do
										  link "#{document.file_identifier}", "#{ENV['DOMAIN']}#{document.file.url}"
										end
										docx.p
									end
								end
							end
						end
						docx.p
					end

					docx.h2 'Note'
					docx.p "This document was created on #{Date.today.strftime('%B %d, %Y')}", italic: true
				end
				@report.report_documents.create(document: Pathname.new("#{Rails.root}/public/uploads/reports/#{filename}").open)
				File.delete("#{Rails.root}/public/uploads/reports/#{filename}")
			end
			
			if @report.send_report.include? "email_report"
				ReportMailer.email_report(@report).deliver rescue ""
			end
			
			if @report.send_report.include? "download_report"
				report_documents = @report.report_documents
				if report_documents.size > 1
					filename = "#{@report.id}-#{Time.now.to_i}.zip"
					temp_file = Tempfile.new(filename)
					begin
						# Initialize the temp file as a zip file
		  			Zip::OutputStream.open(temp_file) { |zos| }
						# Add files to the zip file
						Zip::File.open(temp_file.path, Zip::File::CREATE) do |zipfile|
							report_documents.each do |report_document|
								zipfile.add(report_document.document.file.file.split("/").last, report_document.document.path)
							end
						end
						# Read the binary data from the file
		  			zip_data = File.read(temp_file.path)
						# Sending the data to the browser as an attachment
					  send_data(zip_data, type: 'application/zip', filename: filename)
					ensure
					  # Close and delete the temp file
					  temp_file.close
					  temp_file.unlink
					end
				else
					send_file report_documents.last.document.path
				end
			else
				redirect_to root_path
			end
		else
			redirect_to root_path
		end
	end

	def destroy
		@destroy = @report.destroy
		get_reports if @destroy
	end
	
	def get_reports
		@reports = current_user.reports.order("id ASC")
	end
	
	private
		def get_report
			@report = current_user.reports.find(params[:id]) rescue ""
      unless @report.present?
        redirect_to root_path
      end
		end
		
		def get_all_sites
			@sites = Site.eager_load(:site_tabs).where(user_id: current_user.id, active: true).order("sites.id ASC")
		end
		
		def report_params
			params.require(:report).permit(:site_id, :sites, :report_type, :document_expires, :lock_document, {tabs: [], items: [], documents: [], report_format: [], send_report: [], to_email: [], cc_email: [], reports: []})
		end
end
