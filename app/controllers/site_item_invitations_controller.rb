class SiteItemInvitationsController < ApplicationController
  ## Filters
  before_action :authenticate_user!
  before_action :get_site, only: [:invitations, :save_invitation, :send_invitations, :delete_invitations, :get_invitations]
  before_action :get_site_item_invitation, only: [:pause_invitation, :delete_invitation, :update_invitation]
  skip_before_action :get_static_page, except: [:invitations]
  
  def invitations
    @site_tabs = @site.site_tabs.eager_load(:site_items).order("site_tabs.id ASC, site_items.id ASC")
    @site_tab = @site_tabs.select {|site_tab| site_tab.id == params[:site_tab_id].to_i}.first rescue ""
  end
  
  def save_invitation
    @site_item = @site.site_items.eager_load(:site_item_invitations).order("site_item_invitations.id ASC").find(params[:site_item_id]) rescue ""
    @saved = false
    if @site_item.present?
      if @site_item.site_item_invitations.where(upload_access_email: site_item_invitation_params[:upload_access_email]).last.blank?
        site_item_invitation = @site_item.site_item_invitations.new(site_item_invitation_params)
        already_user = User.user_email(site_item_invitation.upload_access_email)
        if already_user.present?
          roles = already_user.roles.pluck(:name)
          if roles.include? "contractor"
            save_site_item_invitation(site_item_invitation)
          end
        else
          save_site_item_invitation(site_item_invitation)
        end
      end
    end
  end
  
  def save_site_item_invitation(site_item_invitation)
    if site_item_invitation.save
      @site_item_invitations = @site_item.site_item_invitations
      @saved = true
    end
  end
  
  def pause_invitation
    if @site_item_invitation.pause?
      @saved = @site_item_invitation.update(pause: false)
    else
      @saved = @site_item_invitation.update(pause: true)
    end
  end
  
  def delete_invitation
    @site_item_invitation_id = @site_item_invitation.id
    @destroy = @site_item_invitation.destroy
  end
  
  def update_invitation
    @saved = @site_item_invitation.update(site_item_invitation_params)
  end
  
  def send_invitations
    @site.site_items.eager_load(:site_item_invitations).each do |site_item|
      site_item_invitations = site_item.site_item_invitations
      site_item_invitations.each do |site_item_invitation|
        unless site_item_invitation.invitation_sent?
          already_user = User.user_email(site_item_invitation.upload_access_email)
          if already_user.present?
            roles = already_user.roles.pluck(:name)
            if roles.include? "contractor"
              begin   
                SiteMailer.document_invitation(site_item_invitation, site_item, @site, current_user).deliver_now
                SiteMailer.invitation_confirmation(site_item_invitation, site_item, @site, current_user).deliver_now
              rescue => e
                Rails.logger.info "SiteMailer - invitation_confirmation, document_invitation: #{e}"
              end
            end
          else
            user = User.invite!(email: site_item_invitation.upload_access_email) do |u|
              u.skip_invitation = true
              u.add_role :contractor
            end
            token = user.raw_invitation_token
            begin   
              SiteMailer.document_invitation(site_item_invitation, site_item, @site, current_user, token).deliver_now
              SiteMailer.invitation_confirmation(site_item_invitation, site_item, @site, current_user).deliver_now
            rescue => e
              Rails.logger.info "SiteMailer - invitation_confirmation, invitation_confirmation: #{e}"
            end
          end
          site_item_invitation.update(invitation_sent: true)
        end
      end
    end
  end
  
  def delete_invitations
    @site.site_items.eager_load(:site_item_invitations).each do |site_item|
      site_item_invitations = site_item.site_item_invitations
      site_item_invitations.destroy_all if site_item_invitations.present?
    end
  end
  
  def get_invitations
    @site_tab = @site.site_tabs.find(params[:site_tab_id]) rescue ""
    @site_items = @site_tab.site_items.where(site_id: @site.id).eager_load(:site_item_questions, :site_item_invitations).order("site_items.id ASC, site_item_questions.id ASC")
  end
  
  private
    def get_site
      @site = Site.get_site(params[:id])
      unless @site.present?
        redirect_to root_path
      end
    end
    
    def get_site_item_invitation
      @site_item_invitation = SiteItemInvitation.find(params[:id]) rescue ""
      unless @site_item_invitation.present?
        redirect_to root_path
      end
    end
    
    def site_item_invitation_params
      params.require(:site_item_invitation).permit(:user_id, :site_id, :upload_access_email, :access_expires, :access_expires_date, :comment)
    end
end
