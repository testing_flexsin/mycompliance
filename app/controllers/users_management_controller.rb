class UsersManagementController < ApplicationController
	# Filters
	before_action :authenticate_user!, :check_user_role
	before_action :get_manager, except: [:index, :add_manager, :create_manager]

	def index
		get_managers
  end
	
	def add_manager
		@manager = User.new
	end
	
	def create_manager
		params[:user][:permissions] = params[:user][:permissions].reject!(&:empty?).join(',') if params[:user][:permissions].present?
		manager = current_user.managers.new(user_params)
		manager.skip_confirmation!
		@saved = manager.save
		if @saved
			manager.add_role :manager
			password = params[:user][:password] rescue ""
			begin    
				UserMailer.manager_created(current_user, manager, password).deliver_now
	    	UserMailer.manager_account(manager, password).deliver_now
  		rescue => e
    		Rails.logger.info "UserMailer - manager_created, manager_account: #{e}"
  		end
			get_managers
		end
	end
	
	def edit_manager
	end
		
	def update_manager
		params[:user][:permissions] = params[:user][:permissions].reject!(&:empty?).join(',') if params[:user][:permissions].present?
		password = params[:user][:password] rescue ""
		if password.present? && (password.length >= 8) && !@manager.valid_password?(password)
			@saved = @manager.update_with_password(user_params)
			password_updated = true
		else
			@saved = @manager.update_without_password(user_params)
			password_updated = false
		end
		if @saved
			begin    
				UserMailer.manager_updated(@manager, password, password_updated).deliver_now
  		rescue => e
    		Rails.logger.info "UserMailer - manager_updated: #{e}"
  		end
			get_managers
		end
	end
	
	def update_status
		if @manager.active?
			@saved = @manager.update(active: false)
		else
			@saved = @manager.update(active: true)
		end
	end
	
	def delete_manager
		@destroy = @manager.destroy
	end
	
	private
		def check_user_role
			unless current_user.has_role? :company
				redirect_to root_path
			end
		end
		
		def get_managers
			@managers = current_user.managers.order("id ASC")
		end
		
		def get_manager
			@manager = current_user.managers.find(params[:user_id]) rescue ""
			if @manager.blank?
				redirect_to root_path
			end
		end
		
		def user_params
			params.require(:user).permit(:first_name, :last_name, :landline_number, :job_title, :mobile_number, :email, :user_name, :password, :password_confirmation, :employee_id, :avatar, :permissions)
		end
end
