class HomeController < ApplicationController
  ## Filters
  skip_before_action :get_static_page, only: [:create_comment, :new_reply, :create_reply, :save_contact_us]
  before_action :get_news, only: [:news_show, :create_comment]
  before_action :news_type, only: [:news, :news_show]
  
  def index
    @banners = BannerImage.where(active: true).order("id ASC") rescue ""
    @services = Service.where(show_on_home: true).order("id ASC") rescue ""
    @teams = Team.where(show_on_home: true).order("id ASC") rescue ""
    @news = News.where(show_on_home: true).order("id ASC") rescue ""
    @testimonials = Testimonial.where(active: true).order("id ASC") rescue ""
  end

  def about_us
  end

  def pricing
  end

  def faq
  end

  def why_us
  end

  def terms_and_conditions
  end

  # news
  def news
    archive_date = Date.parse(params[:archive]) rescue ""
    if params[:search].present?
      @news = News.where("title ILIKE :search", search: "%#{params[:search]}%")
    elsif params[:archive].present? && archive_date.present?
      @news = News.where("created_at >= ? AND created_at <= ?", archive_date.to_date.beginning_of_month, archive_date.to_date.end_of_month)
    else
      @news = News.order("id DESC")
    end
  end

  def news_show
    @comments = @news.comments.hash_tree(limit_depth: 7)
    @comment = @news.comments.new 
  end

  # send inquiry
  def inquiry
    @inquiry = Inquiry.new
  end

  def create_newsletter
    @message = ""
    @newsletter = Newsletter.new(newsletter_params)
    @saved = @newsletter.save
    if @saved
      begin   
        AdminMailer.send_newsletter_email(@newsletter).deliver_now
      rescue => e
        Rails.logger.info "AdminMailer - send_newsletter_email: #{e}"
      end
    end
  end

  def create_inquiry
    @inquiry = Inquiry.new(inquiry_params)
    @saved = @inquiry.save
    if @saved
      begin   
        AdminMailer.send_inquiry_email(@inquiry).deliver_now
      rescue => e
        Rails.logger.info "AdminMailer - send_inquiry_email: #{e}"
      end
    end
    redirect_to :back
  end

  # features
  def features
    @features = Feature.order("id DESC")
  end

  def feature_show
    @feature = Feature.find(params[:id])
    @features = Feature.all_except(@feature) rescue ""
    if @feature.blank?
      redirect_to root_path
    end

  end

  # services
  def services
    @services = Service.order("id DESC")
  end

  def service_show
    @service = Service.find(params[:id]) rescue ""
    if @service.blank?
      redirect_to root_path
    end
  end

  # team
  def teams
    @teams = Team.order("id ASC")
  end

  def team_show
    @team = Team.find(params[:id]) rescue ""
    @teams = Team.all_except(@team) rescue ""
    if @team.blank?
      redirect_to root_path
    end
  end

  # comment
  def create_comment
    @comment = @news.comments.new(comment_params)
    @saved = @comment.save 
    news_type
  end

  # reply
  def new_reply
    @comment = Comment.find(params[:id]) rescue ""
  end

  def create_reply
    @comment = Comment.new(comment_params)
    @saved = @comment.save
    @comments = @comment.news.comments.hash_tree
  end

  # contact_us
  def contact_us
    @contact = ContactUs.new
  end

  def save_contact_us
    @message = ""
    @saved = false
    contact = ContactUs.new(contact_params)
    if contact.save
      begin   
        AdminMailer.contact_us(contact).deliver_now
      rescue => e
        Rails.logger.info "AdminMailer - contact_us: #{e}"
      end
      @saved = true
      @message = "We will get back to you soon."
    else
      @message = "Sorry please try again."
    end
  end

  def privacy_policy
  end

  def news_search
    @news = News.where("title like :search",{search: "%#{search}%"})
  end 

  private
    def get_news
      @news = News.find(params[:id]) rescue ""
      if @news.blank?
        redirect_to root_path
      end
    end

    def news_type
      @popular_news = News.joins(:comments).select('news.*, count(comments.id) as comment_count').group("news.id").order("comment_count DESC").first(3)
      if @popular_news.size <= 2
        @latest_popular_news = News.where.not(id: @popular_news.select{|n| n.id}.uniq).order("created_at DESC").first(3-@popular_news.size)
      end
      @archive = News.all.group_by {|u| u.created_at.beginning_of_month }.keys
      @latest_news = News.order("created_at DESC").first(3)
    end
    
    def contact_params
      params.require(:contact_us).permit(:name, :email, :contact_number, :message)
    end
    
    def comment_params
      params.require(:comment).permit(:news_id, :parent_id, :user_name, :email, :message)
    end

    def inquiry_params
      params.require(:inquiry).permit(:name, :email, :contact_number, :message)
    end

    def newsletter_params
      params.require(:newsletter).permit(:name, :email)
    end
end
