class UsersController < ApplicationController
	# Filters
	before_action :authenticate_user!, except: [:paypal_callback, :check_username_unique, :check_email_unique]
	protect_from_forgery except: [:paypal_callback]
	skip_before_action :get_static_page, except: [:settings, :update]
	skip_before_action :check_profile, only: [:update, :update_profile, :get_states, :get_cities]

	def settings
		if user_permission("user_settings")
			@email_notification = current_user.user_email_notification rescue ""
			if @email_notification.blank?
				@email_notification = current_user.build_user_email_notification
			end
		else
			redirect_to root_path
		end
	end

	def user_email_notification
		@email_notification = current_user.user_email_notification rescue ""
		if @email_notification.present?
			@saved = @email_notification.update(email_notification_params)
		else
			@email_notification = current_user.build_user_email_notification(email_notification_params)
			@saved = @email_notification.save
		end
	end
	
	def update
		@company_information = ""
		if current_role.include?("company")
			@organization_types = OrganizationType.select(:id, :org_type)
			@number_of_employees = NumberOfEmployee.select(:id, :employees)
			@company_information = current_user.company_information rescue ""
			@countries = CS.countries
			if @company_information.present?
				@states = CS.states(@company_information.country)
				@cities = CS.cities(@company_information.state, @company_information.country)
			else
				@states = {}
				@cities = {}
				@company_information = current_user.build_company_information
			end
		end
	end
	
	def get_states
		country_id = params[:country_id]
		@states = CS.states(country_id)
	end
	
	def get_cities
		country_id = params[:country_id]
		state_id = params[:state_id]
		@cities = CS.cities(state_id, country_id)
	end

	def paypal_callback
  	@user = User.find(params[:invoice]) rescue ""
  	if @user.present?
	    @transaction_detail = @user.transaction_details.new(
				plan_id: params[:custom],
				payer_id: params[:payer_id],
				payer_email: params[:payer_email],
				payment_date: params[:payment_date],
				payment_status: params[:payment_status],
				payment_fee: params[:payment_fee],
				payment: params[:payment_gross],
				payment_type: params[:payment_type],
				mc_currency: params[:mc_currency],
				first_name: params[:first_name],
				last_name: params[:last_name],
				txn_id: params[:txn_id],
				receiver_id: params[:receiver_id],
				receiver_email: params[:receiver_email],
				address_name: params[:address_name],
				address_street: params[:address_street],
				address_city: params[:address_city],
				address_state: params[:address_state],
				address_country: params[:address_country],
				address_country_code: params[:address_country_code],
				address_zip: params[:address_zip]
	    )
	  end

		if @transaction_detail.present? && @transaction_detail.payment_status.present? && (@transaction_detail.payment_status == "Completed")
			if @transaction_detail.save
				@user.update(plan_id: @transaction_detail.plan_id)
			end
			redirect_to new_user_session_path
		else
			redirect_to new_user_registration_path
		end
	end
	
	def update_profile
		@saved = current_user.update(user_profile_params)
	end

	def update_user
		@saved = current_user.update(user_params)
	end

	def delete_logo
		current_user.remove_avatar!
	end
	
	def check_username_unique
		unique = true
    user_name = params[:user][:user_name] rescue ""
    if user_name.present?
      user = User.user_username(user_name)
      if user.present?
        unique = false
      else
        if( (user_name == "super") || (user_name == "super_admin") || (user_name == "admin") || (user_name == "user") || (user_name == "admin_user") || (user_name == "user_admin") )
          unique = false
        end
      end
    end
		render json: unique
  end

  def check_current_username_unique
		unique = true
    user_name = params[:user][:user_name] rescue ""
    if user_name.present?
      user = User.user_username(user_name)
      if user.present?
				old_username = params[:username]
				if old_username.present?
					old_user = User.user_username(old_username)
					if user.id != old_user.id
	        	unique = false
					end
				else
					if user.id != current_user.id
	        	unique = false
					end
				end
      else
        if( (user_name == "super") || (user_name == "super_admin") || (user_name == "admin") || (user_name == "user") || (user_name == "admin_user") || (user_name == "user_admin") )
          unique = false
        end
      end
    end
		render json: unique
  end

  def check_email_unique
		unique = true
    email = params[:user][:email] rescue ""
    if email.present?
      user = User.user_email(params[:user][:email])
      if user.present?
        unique = false
      end
    end
		render json: unique
  end

  def check_current_email_unique
		unique = true
    email = params[:user][:email] rescue ""
    if email.present?
      user = User.user_email(params[:user][:email])
      if user.present?
				old_email = params[:email]
				if old_email.present?
					old_user = User.user_email(old_email)
					if user.id != old_user.id
	        	unique = false
					end
				else
					if user.id != current_user.id
	        	unique = false
					end
				end
      end
    end
		render json: unique
  end

	private
		def email_notification_params
			params.require(:user_email_notification).permit(:due_soon, :due_soon_often, :overdue, :overdue_often, :due_soon_invitee, :due_soon_often_invitee, :overdue_invitee, :overdue_often_invitee)
		end

		def user_params
			params.require(:user).permit(:time_zone, :avatar)
		end
		
		def user_profile_params
			params.require(:user).permit(:email, :first_name, :last_name, :mobile_number, :landline_number, company_information_attributes: [:id, :user_id, :name, :city, :state, :country, :zip_code, :organization_type, :number_of_employees, :logo, :_destroy])
		end
end

