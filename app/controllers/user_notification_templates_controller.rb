class UserNotificationTemplatesController < ApplicationController
	# Filters
	before_action :authenticate_user!
	skip_before_action :get_static_page, except: [:index]

	# Actions
	def index
		if ((current_role.include? "manager") && current_user.permissions.present? && (current_user.permissions.include? "notification")) || (current_role.include? "company")
			if current_user.user_notification_template.present?
				@notification_template = current_user.user_notification_template rescue ""
			else
				@notification_template = current_user.build_user_notification_template
			end
		else
			redirect_to root_path
		end
	end

	def create
		@notification_template = current_user.build_user_notification_template(notification_templates_params)
		@saved = @notification_template.save
	end

	def update
		if current_user.user_notification_template.present?
			@notification_template = current_user.user_notification_template rescue ""
			@saved =  @notification_template.update(notification_templates_params)
		else
			@notification_template = current_user.build_user_notification_template(notification_templates_params)
			@saved = @notification_template.save
		end
	end

	private
		def notification_templates_params
			params.require(:user_notification_template).permit(:invitee_template, :email_due_soon_template, :email_overdue_template, :email_due_soon_invitee_template, :email_overdue_invitee_template, :email_report_recipients_template, :document_feedback_approved_template, :document_feedback_pending_template, :document_feedback_rejected_template)
		end
end
