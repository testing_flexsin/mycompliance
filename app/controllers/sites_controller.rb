class SitesController < ApplicationController
  layout "dashboard"
  
  ## Filters
  before_action :authenticate_user!
  before_action :get_site, only: [:edit, :destroy_show, :destroy, :new_site_item, :save_site_item, :update_site_item, :delete_site_item, :get_site_items, :duplicate_last_item, :get_site_item_templates, :add_item_from_template, :see_more, :check_tab_name_unique]
  before_action :get_site_with_relation, only: [:update]
  before_action :get_extra_params, only: [:index, :new, :create, :edit, :add_site_from_template, :duplicate_existing_site, :update, :destroy]
  skip_before_action :get_static_page, except: [:index, :new, :edit]
  
  def index
    count = params[:count].to_i rescue 10
		if count.present? && (count >= 10)
			current_user.update(sites_per_page: count)
		end
    order_by = "id"
    order_dir = "ASC"
    if @order.present?
      case @order
      when "due_date_asc"
        order_by = "annual_compliance_due"
        order_dir = "ASC"
      when "due_date_desc"
        order_by = "annual_compliance_due"
        order_dir = "DESC"
      when "name_asc"
        order_by = "name"
        order_dir = "ASC"
      when "name_desc"
        order_by = "name"
        order_dir = "DESC"
      end
    end
    sites_condition = ""
    if @search.present?
      sites_condition += "sites.name LIKE '%#{@search}%'"
    end
    @from_date = Date.parse(params[:from_date]) rescue nil
    if @from_date.present?
      sites_condition += " AND " if sites_condition.present?
      sites_condition += "DATE(sites.created_at) >= '#{@from_date.to_date}'"
    end
    @to_date = Date.parse(params[:to_date]) rescue nil
    if @to_date.present?
      sites_condition += " AND " if sites_condition.present?
      sites_condition += "DATE(sites.created_at) <= '#{@to_date.to_date}'"
    end
    if current_role.include? "contractor"
      @site_item_invitations = SiteItemInvitation.includes(:site, :site_item).get_site_item_invitations(current_user)
      @sites, @site_tabs, @site_items = [], [], []
      @site_item_invitations.each do |site_item_invitation|
        site = site_item_invitation.site
        @sites << site if site.present?
        site_item = site_item_invitation.site_item
        if site_item.present?
          @site_items << site_item
          site_tab = site_item.site_tab
          if site_tab.present? && !(@site_tabs.include? site_tab.name)
            @site_tabs << site_tab.name
          end
        end
      end
      @site_tabs.sort!
      unless @site_tabs.include? @tab
        @tab = @site_tabs.first
      end
      page = params[:page].present? ? params[:page] : "1"
      @sites = Site.eager_load(:site_tabs, :site_items).where(id: @sites.collect(&:id), site_tabs: {name: @tab}).where(sites_condition).paginate(page: page, per_page: current_user.sites_per_page).order("sites.#{order_by} #{order_dir}")
    else
      get_site_tabs
      @site_tabs.sort!
      unless @site_tabs.include? @tab
        @tab = @site_tabs.first
      end
      get_sites(@tab, order_by, order_dir, sites_condition)
      @upload_notifications = current_user.upload_notifications.unread
    end
  end

  def new
    @site = current_user.sites.new
    @site_setting = SiteSetting.last
    @sites = Site.current_sites(current_user.id)
    @site_templates = Site.templates.select(:id, :name)
    @item_templates = SiteItem.templates(current_user.id).select(:id, :title)
  end

  def edit
    @site_tabs = @site.site_tabs.eager_load(:site_items).order("site_tabs.id ASC, site_items.id ASC")
    @site_tab = @site_tabs.select {|site_tab| site_tab.name == @tab}.first rescue ""
  end

  def create
    @site = current_user.sites.new(site_params)
    if @site.save
      tabs = @site.tabs
      tab_items = @site.tab_items
      (1..tabs).each do |tab|
        site_tab = @site.site_tabs.new(user_id: current_user.id, name: "Tab #{tab}", items: tab_items)
        site_item = SiteItem.find(@site.site_item_id) rescue ""
        if site_tab.save && site_item.present?
          (1..tab_items).each do |item|
            new_site_item = site_item.dup
            new_site_item.site_id = @site.id
            new_site_item.site_tab_id = site_tab.id
            new_site_item.template = false
            if new_site_item.save
              site_item.site_item_questions.each do |site_item_question|
                new_site_item_question = site_item_question.dup
                new_site_item_question.site_id = @site.id
                new_site_item_question.site_item_id = new_site_item.id
                new_site_item_question.save
              end
            end
          end
        end
      end
      @site_tabs = @site.site_tabs.eager_load(:site_items).order("site_tabs.id ASC, site_items.id ASC")
      @site_tab = @site_tabs.first rescue ""
      @saved = true
    else
      @saved = false
    end
  end
  
  def add_site_from_template
    @site = Site.find(params[:site_id]) rescue ""
    if @site.present? && @site.template?
      @new_site = @site.dup
      @new_site.user_id = current_user.id
      @new_site.template = false
      if @new_site.save
        @site.site_tabs.each do |site_tab|
          new_site_tab = site_tab.dup
          new_site_tab.user_id = current_user.id
          new_site_tab.site_id = @new_site.id
          if new_site_tab.save
            site_tab.site_items.each do |site_item|
              new_site_item = site_item.dup
              new_site_item.user_id = current_user.id
              new_site_item.site_id = @new_site.id
              new_site_item.site_tab_id = new_site_tab.id
              if new_site_item.save
                site_item.site_item_questions.each do |site_item_question|
                  new_site_item_question = site_item_question.dup
                  new_site_item_question.site_id = @new_site.id
                  new_site_item_question.site_item_id = new_site_item.id
                  new_site_item_question.save
                end
              end
            end
          end
        end
      end
      @site_tabs = @new_site.site_tabs.eager_load(:site_items).order("site_tabs.id ASC, site_items.id ASC")
      @site_tab = @site_tabs.first rescue ""
      @saved = true
    else
      @saved = false
    end
  end
  
  def duplicate_existing_site
    @site = Site.find(params[:site_id]) rescue ""
    if @site.present?
      @new_site = @site.dup
      @new_site.name = "Copy " + @new_site.name
      if @new_site.save
        @site.site_tabs.each do |site_tab|
          new_site_tab = site_tab.dup
          new_site_tab.site_id = @new_site.id
          if new_site_tab.save
            site_tab.site_items.each do |site_item|
              new_site_item = site_item.dup
              new_site_item.site_id = @new_site.id
              new_site_item.site_tab_id = new_site_tab.id
              if new_site_item.save
                site_item.site_item_questions.each do |site_item_question|
                  new_site_item_question = site_item_question.dup
                  new_site_item_question.site_id = @new_site.id
                  new_site_item_question.site_item_id = new_site_item.id
                  new_site_item_question.save
                end
              end
            end
          end
        end
      end
      @site_tabs = @new_site.site_tabs.eager_load(:site_items).order("site_tabs.id ASC, site_items.id ASC")
      @site_tab = @site_tabs.first rescue ""
      @saved = true
    else
      @saved = false
    end
  end
  
  def update
    if @site.update(site_params)
      @site_tabs = @site.site_tabs.eager_load(:site_items).order("site_tabs.id ASC, site_items.id ASC")
      @site_tabs.each do |site_tab|
        if site_tab.user_id.blank?
          site_tab.update(user_id: current_user.id)
        end
      end
      @site_tab = @site_tabs.select {|site_tab| site_tab.name == @tab}.first rescue ""
      @saved = true
    else
      @saved = false
    end
  end

  def destroy_show
  end

  def destroy
    if @site.destroy
      order_by = "id"
      order_dir = "ASC"
      if @order.present?
        case @order
        when "due_date_asc"
          order_by = "annual_compliance_due"
          order_dir = "ASC"
        when "due_date_desc"
          order_by = "annual_compliance_due"
          order_dir = "DESC"
        when "name_asc"
          order_by = "name"
          order_dir = "ASC"
        when "name_desc"
          order_by = "name"
          order_dir = "DESC"
        end
      end
      sites_condition = ""
      if @search.present?
        sites_condition += "sites.name LIKE '%#{@search}%'"
      end
      @from_date = Date.parse(params[:from_date]) rescue nil
      if @from_date.present?
        sites_condition += " AND " if sites_condition.present?
        sites_condition += "DATE(sites.created_at) >= '#{@from_date.to_date}'"
      end
      @to_date = Date.parse(params[:to_date]) rescue nil
      if @to_date.present?
        sites_condition += " AND " if sites_condition.present?
        sites_condition += "DATE(sites.created_at) <= '#{@to_date.to_date}'"
      end
      
      get_site_tabs
      @site_tabs.sort!
      unless @site_tabs.include? @tab
        @tab = @site_tabs.first
      end
      get_sites(@tab, order_by, order_dir, sites_condition)
      @destroy = true
    else
      @destroy = false
    end
  end
  
  def new_site_item
    @site_tab = @site.site_tabs.find(params[:site_tab_id]) rescue ""
    @site_item = @site_tab.site_items.new
    @site_item.site_item_questions.new
  end
  
  def save_site_item
    @site_item = @site.site_items.new(site_item_params)
    @site_item.user_id = current_user.id
    if @site_item.save
      @site_items_size = @site.site_items.where(site_tab_id: @site_item.site_tab_id).count rescue 1
      @saved = true
    else
      @saved = false
    end
  end
  
  def update_site_item
    @site_item = @site.site_items.find(params[:site_item_id]) rescue ""
    if @site_item.update(site_item_params)
      @site_items_size = @site.site_items.where(site_tab_id: @site_item.site_tab_id).count rescue 1
      @saved = true
    else
      @saved = false
    end
  end
  
  def delete_site_item
    @site_item = @site.site_items.find(params[:site_item_id]) rescue ""
    @site_item_id = @site_item.id
    if @site_item.present? && @site_item.destroy
      @destroy = true
    else
      @destroy = false
    end
  end
  
  def get_site_items
    @site_tabs = @site.site_tabs.eager_load(:site_items).order("site_tabs.id ASC, site_items.id ASC")
    @site_tab = @site.site_tabs.find(params[:site_tab_id]) rescue ""
  end
  
  def duplicate_last_item
    site_item = @site.site_items.where(id: params[:site_item_id], site_tab_id: params[:site_tab_id]).last rescue ""
    if site_item.present?
      @new_site_item = site_item.dup
      if @new_site_item.save
        site_item.site_item_questions.each do |site_item_question|
          new_site_item_question = site_item_question.dup
          new_site_item_question.site_id = @site.id
          new_site_item_question.site_item_id = @new_site_item.id
          new_site_item_question.save
        end
        @site_items_size = @site.site_items.where(site_tab_id: params[:site_tab_id]).count rescue 1
        @saved = true
      else
        @saved = false
      end
    end
  end
  
  def get_site_item_templates
    @item_templates = SiteItem.templates(current_user.id).select(:id, :title)
    @site_tab_id = params[:site_tab_id]
  end
  
  def add_item_from_template
    site_item = SiteItem.where(id: params[:site_item_template_id], template: true).last rescue ""
    if site_item.present?
      @new_site_item = site_item.dup
      @new_site_item.site_id = @site.id
      @new_site_item.site_tab_id = params[:site_tab_id]
      @new_site_item.template = false
      if @new_site_item.save
        site_item.site_item_questions.each do |site_item_question|
          new_site_item_question = site_item_question.dup
          new_site_item_question.site_id = @site.id
          new_site_item_question.site_item_id = @new_site_item.id
          new_site_item_question.save
        end
        @site_items_size = @site.site_items.where(site_tab_id: @new_site_item.site_tab_id).count rescue 1
        @saved = true
      else
        @saved = false
      end
    end
  end
  
  def upload_site_item_file
    @site = Site.get_site(params[:id])
    @site_item = @site.site_items.find(params[:site_item_id])
    @site_item_document = @site_item.site_item_documents.new
  end
  
  def create_site_item_file
    @site = Site.get_site(params[:id])
    @site_item = @site.site_items.find(params[:site_item_id])
    @site_item_document = @site_item.site_item_documents.new(site_item_document_params)
    @site_item_document.user_id = current_user.id
    @site_item_document.site_id = @site.id
    @site_item_document.contractor_email = current_user.email
    if @site_item_document.save
      @site.update(status: "New Upload")
      @site_item.update(status: "New Upload")
      @upload_notification = UploadNotification.create(user_id: @site.user_id, site_item_document_id: @site_item_document.id, body: "New Upload")
      @upload_notifications = @upload_notification.user.upload_notifications.unread
      @saved = true
    else
      @saved = false
    end
  end
  
  def see_more
    @site_tab = @site.site_tabs.eager_load(:site_items).find(params[:site_tab_id]) rescue ""
    @site_items = @site_tab.site_items.eager_load(:site_item_questions).order("site_items.id ASC, site_item_questions.id ASC")
    @site_item_id = params[:site_item] rescue ""
    @item_index = params[:index] rescue ""
  end
  
  def site_item_question_compliant
    @site_item_question = SiteItemQuestion.find(params[:id]) rescue ""
    if @site_item_question.present? && @site_item_question.compliant?
      @site_item_question.update(compliant: false)
    elsif @site_item_question.present? && !@site_item_question.compliant?
      @site_item_question.update(compliant: true)
    end
    @site_item = @site_item_question.site_item rescue ""
    @replace_item = false
    if @site_item.present? && @site_item.required_for_site_compliance?
      site_item_questions = @site_item.site_item_questions.where(required_for_compliant: true)
      if site_item_questions.size == (site_item_questions.select{|site_item_question| site_item_question.compliant?}.size)
        @replace_item = true if @site_item.update(compliant: true)
      end
    end
    @site = @site_item_question.site rescue ""
    @replace_site = false
    if @site.present?
      site_items = @site.site_items.where(required_for_site_compliance: true)
      if site_items.size == (site_items.select{|site_item| site_item.compliant?}.size)
        @replace_site = true if @site.update(compliant: true)
      end
    end
    @site_tab = @site.site_tabs.find(params[:tab]) rescue ""
    if @replace_item
      @index = 0
      site_items = @site_tab.site_items.where(site_id: @site.id).order("site_items.id ASC")
      site_items.each.with_index do |site_item, index|
        if site_item == @site_item
          @index = index
        end
      end
    end
    get_sites_size
  end
  
  def check_tab_name_unique
    unique = true
    tab_name = params[:tab_name] rescue ""
    if tab_name.present?
      site_tabs = @site.site_tabs.where("name ILIKE ?", tab_name) rescue ""
      if site_tabs.present?
        unique = false
      end
    end
		render json: unique
  end
  
  private
    def get_site_tabs
      @site_tabs = current_user.site_tabs.pluck(:name).uniq
    end
    
    def get_sites(tab, order_by="id", order_dir="ASC", sites_condition="")
      @page = params[:page].present? ? params[:page] : "1"
      @sites = Site.current_sites(current_user.id, @page, current_user.sites_per_page, order_by, order_dir).where(sites_condition).site_tabs(tab)
      get_sites_size
    end
    
    def get_sites_size
      sites = current_user.sites
      @compliant_sites = sites.select {|site| site.compliant?}.size rescue 0
      @not_compliant_sites = sites.select {|site| !site.compliant?}.size rescue 0
      @due_compliant_sites = 0
      sites.each do |site|
        annual_time = (site.annual_compliance_due.to_datetime - DateTime.now).to_i
        @due_compliant_sites += 1 if (annual_time >= 0) && (annual_time < 30)
      end
    end
    
    def get_site_with_relation
      @site = Site.get_site_with_relation(params[:id])
      check_site
    end
    
    def get_site
      @site = Site.get_site(params[:id])
      check_site
    end
    
    def check_site
      if !@site.present? || (@site.user != current_user)
        redirect_to root_path
      end
    end
    
    def get_extra_params
      @tab = params[:tab].present? ? params[:tab] : nil
      @page = params[:page].present? ? params[:page] : nil
      @order = params[:order].present? ? params[:order] : nil
      @search = params[:search].present? ? params[:search] : nil
      @from_date = params[:from_date].present? ? params[:from_date] : nil
      @to_date = params[:to_date].present? ? params[:to_date] : nil
    end

    def site_params
      params.require(:site).permit(:user_id, :name, :annual_compliance_due, :tabs, :tab_items, :site_item_id, site_tabs_attributes: [:id, :user_id, :site_id, :name, :_destroy])
    end

    def site_item_params
      params.require(:site_item).permit(:user_id, :site_id, :site_tab_id, :title, :frequency, :required_for_site_compliance, :due_date, :due_soon, site_item_questions_attributes: [:id, :site_id, :question, :required_for_compliant, :_destroy])
    end
    
    def site_item_document_params
      params.require(:site_item_document).permit(:site_id, :site_item_id, :user_id, :file)
   	end
end
