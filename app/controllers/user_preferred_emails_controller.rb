class UserPreferredEmailsController < ApplicationController
	## Filters
	before_action :authenticate_user!
	skip_before_action :get_static_page, except: [:index]
	before_action :get_preferred_email, only: [:show, :destroy, :update, :pause]

	def index
		if user_permission("preferred_emails")
			get_preferred_emails
			@preferred_email = current_user.user_preferred_emails.new 
		else
			redirect_to root_path
		end
	end

	def create
		@preferred_email = current_user.user_preferred_emails.new(preferred_emails_params) rescue ""
		@saved = @preferred_email.save
		get_preferred_emails if @saved
	end

	def update
	end

	def destroy
		@destroy = @preferred_email.destroy
		get_preferred_emails if @destroy
	end

	def pause
		if @preferred_email.pause == true
			@preferred_email = @preferred_email.update_attributes(pause: false)
		else
			@preferred_email = @preferred_email.update_attributes(pause: true)
		end
		get_preferred_emails
	end

	def import
	  UserPreferredEmail.import(params[:file], current_user.id)
	  get_preferred_emails
	end

	def get_preferred_emails
	  @preferred_emails = current_user.user_preferred_emails.order("id ASC")
	end

	private
		def get_preferred_email
			@preferred_email= UserPreferredEmail.find(params[:id]) rescue ""
      unless @preferred_email.present?
        redirect_to root_path
      end
		end
		
		def preferred_emails_params
			params.require(:user_preferred_email).permit(:first_name, :last_name, :email)
		end
end
