class SiteItemDocumentsController < ApplicationController
	## Filters
  before_action :authenticate_user!
  before_action :get_site, only: [:documents, :create_document]
  before_action :get_site_item_document, except: [:documents, :create_document]
  skip_before_action :get_static_page, except: [:documents]

	def documents
    if ((current_role.include? "manager") && current_user.permissions.present? && (current_user.permissions.include? "manage_documents")) || (current_role.include? "company")
      @site_item = @site.site_items.find(params[:site_item_id])
      @site_item_invitations = @site_item.site_item_invitations
      @site_item_documents = @site_item.site_item_documents.where(site_id: @site.id) rescue []
      @current_documents = @site_item_documents.select {|document| document.current? && document.approve? } rescue []
      @new_documents = @site_item_documents.select {|document| !document.current? && !document.pending? && !document.archive? && !document.approve? } rescue []
      @pending_documents = @site_item_documents.select {|document| document.pending? || document.archive? } rescue []
      @site_item_document = @site_item.site_item_documents.new
    else
      redirect_to root_path  
    end
  end

  def create_document
    @site_item = @site.site_items.find(params[:site_item_id])
    site_item_documents = @site_item.site_item_documents
    @current_document = site_item_documents.where(current: true, approve: true).last
    
    @site_item_document = site_item_documents.new(site_item_document_params)
    @site_item_document.user_id = current_user.id
    @site_item_document.site_id = @site.id
    @site_item_document.current = true
    @site_item_document.approve = true
    @site_item_document.automatic_archive = true if @current_document.present? && @current_document.automatic_archive?
    @saved = @site_item_document.save
    if @saved
      @current_document.update(current: false, approve: false, pending: false, archive: true) if @current_document.present?
      begin   
        SiteMailer.new_document_upload(@site_item_document, @site_item, @site, current_user).deliver_now if @site_item_document.contractor_email.present?
      rescue => e
        Rails.logger.info "SiteMailer - new_document_upload: #{e}"
      end
    end
  end

  def current_document
    site_item = @site_item_document.site_item
    @current_document = site_item.site_item_documents.where(current: true, approve: true).last
    automatic_archive = (@current_document.present? && @current_document.automatic_archive? ? true : false)
    
    @saved = @site_item_document.update(current: true, approve: true, pending: false, archive: false, automatic_archive: automatic_archive)
    if @saved
      @current_document.update(current: false, approve: false, pending: false, archive: true) if @current_document.present?
      begin   
        SiteMailer.approve_document(@site_item_document, current_user).deliver_now if @site_item_document.contractor_email.present?
      rescue => e
        Rails.logger.info "SiteMailer - approve_document: #{e}"
      end
    end
  end

  def approve_document
    site_item = @site_item_document.site_item
    @current_document = site_item.site_item_documents.where(current: true, approve: true).last rescue ""
    automatic_archive = (@current_document.present? && @current_document.automatic_archive? ? true : false)
    
    @saved = @site_item_document.update(current: true, approve: true, automatic_archive: automatic_archive)
    if @saved
      @current_document.update(current: false, approve: false, pending: false, archive: true) if @current_document.present?
      begin   
        SiteMailer.approve_document(@site_item_document, current_user).deliver_now if @site_item_document.contractor_email.present?
      rescue => e
        Rails.logger.info "SiteMailer - approve_document: #{e}"
      end
    end
  end

  def pending_document
    @saved = @site_item_document.update(pending: true)
  end

  def archive_document
    @saved = @site_item_document.update(current: false, approve: false, archive: true)
  end

  def delete_document
    @site_item_document_id = @site_item_document.id
    @destroy = @site_item_document.destroy
  end

  def reject_document
    @site_item_document_id = @site_item_document.id
    @rejected = @site_item_document.destroy
  end
  
  def due_soon
    if @site_item_document.automatic_archive?
      @site_item_document.update(automatic_archive: false)
    else
      @site_item_document.update(automatic_archive: true)
    end
    render json: true
  end
  
  def feedback
    @ftype = params[:ftype]
  end
  
  def save_feedback
    @saved = false
    @ftype = params[:ftype]
    if @ftype.present? && (@ftype == "pending")
      @saved = @site_item_document.update(pending: true, feedback: site_item_document_params[:feedback])
      if @saved
        @site_item_document.site_item.update(status: "Pending")
      end
      begin   
        SiteMailer.pending_document(@site_item_document, current_user).deliver_now if @site_item_document.contractor_email.present?
      rescue => e
        Rails.logger.info "SiteMailer - pending_document: #{e}"
      end
    elsif @ftype.present? && (@ftype == "reject")
      @site_item_document.update(feedback: site_item_document_params[:feedback])
      begin   
        SiteMailer.reject_document(@site_item_document, current_user).deliver_now if @site_item_document.contractor_email.present?
      rescue => e
        Rails.logger.info "SiteMailer - reject_document: #{e}"
      end
      @site_item_document_id = @site_item_document.id
      @saved = @site_item_document.destroy
    end
  end

	private
  	def get_site
      @site = Site.get_site(params[:id])
      unless @site.present?
        redirect_to root_path
      end
    end
    
    def get_site_item_document
      @site_item_document = SiteItemDocument.find(params[:id]) rescue ""
      unless @site_item_document.present?
        redirect_to root_path
      end
    end

    def site_item_document_params
      params.require(:site_item_document).permit(:site_id, :site_item_id, :user_id, :file, :feedback, :contractor_email)
   	end
end

