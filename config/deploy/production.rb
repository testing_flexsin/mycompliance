role :app, %w{user@IP}
role :web, %w{user@IP}
role :db,  %w{user@IP}

set :stage, :production
set :rails_env, :production

set :deploy_user, "user"

server 'IP', user: 'user', roles: %w{web app db}, primary: true

# set :ssh_options, {
# 	user: 'user',
# 	forward_agent: false,
# 	auth_methods: %w(password),
# 	password: 'password'
# }

set :ssh_options, {
	keys: %w(/home/user/key.pem),
	forward_agent: false,
	auth_methods: %w(publickey)
}