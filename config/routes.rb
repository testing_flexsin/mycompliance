Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users, controllers: { registrations: 'users/registrations', sessions: 'users/sessions', invitations: 'users/invitations' }

  resources :user_preferred_emails, except: [:new, :show, :edit] do
    collection do
      post :import
    end
  end
  patch   '/pause/:id'                                => 'user_preferred_emails#pause',              as: :pause

  resources :user_notification_templates, only: [:index, :create, :update]

  get     '/settings'                                 => 'users#settings',                           as: :my_settings
  post    '/user_email_notification'                  => 'users#user_email_notification',            as: :user_email_notification
  post    '/update_user'                              => 'users#update_user',                        as: :update_user
  delete  '/delete_logo'                              => 'users#delete_logo',                        as: :delete_logo
  get     '/users/update'                             => 'users#update',                             as: :users_update
  post    '/users/update_profile'                     => 'users#update_profile',                     as: :users_update_profile
  get     '/get_states'                               => 'users#get_states',                         as: :get_states
  get     '/get_cities'                               => 'users#get_cities',                         as: :get_cities
  post    '/paypal_callback'                          => 'users#paypal_callback',                    as: :paypal_callback
  get     '/check_username_unique'                    => 'users#check_username_unique',              as: :check_username_unique
  get     '/check_current_username_unique'            => 'users#check_current_username_unique',      as: :check_current_username_unique
  get     '/check_email_unique'                       => 'users#check_email_unique',                 as: :check_email_unique
  get     '/check_current_email_unique'               => 'users#check_current_email_unique',         as: :check_current_email_unique
  
  # Users Management
  get     '/users_management'                         => 'users_management#index',                   as: :users_management
  get     '/users_management/add_manager'             => 'users_management#add_manager',             as: :add_manager
  post    '/users_management/create_manager'          => 'users_management#create_manager',          as: :create_manager
  get     '/users_management/:user_id/edit_manager'   => 'users_management#edit_manager',            as: :edit_manager
  put     '/users_management/:user_id/update_manager' => 'users_management#update_manager',          as: :update_manager
  put     '/users_management/:user_id/update_status'  => 'users_management#update_status',           as: :update_status
  delete  '/users_management/:user_id/delete_manager' => 'users_management#delete_manager',          as: :delete_manager

  resources :sites, except: [:show] do
    member do
      get    :get_site_items
      get    :new_site_item
      post   :save_site_item
      post   :duplicate_last_item
      get    :get_site_item_templates
      post   :add_item_from_template
    end
    collection do
      post   :add_site_from_template
      post   :duplicate_existing_site
    end
  end
  get     '/sites/:id/destroy'                        => 'sites#destroy_show',                       as: :site_destroy_show
  put     '/sites/:id/update_item/:site_item_id'      => 'sites#update_site_item',                   as: :site_update_site_item
  delete  '/sites/:id/delete_item/:site_item_id'      => 'sites#delete_site_item',                   as: :site_delete_site_item
  get     '/sites/:id/upload_file/:site_item_id'      => 'sites#upload_site_item_file',              as: :upload_site_item_file
  post    '/sites/:id/create_file/:site_item_id'      => 'sites#create_site_item_file',              as: :create_site_item_file
  get     '/sites/:id/see_more/:site_tab_id'          => 'sites#see_more',                           as: :site_items_see_more
  get     '/site_item_question/:id/compliant'         => 'sites#site_item_question_compliant',       as: :site_item_question_compliant
  post    '/sites/:id/check_tab_name_unique'          => 'sites#check_tab_name_unique',              as: :check_tab_name_unique
  
  # Site Item Invitations
  get     '/sites/:id/invitations/:site_tab_id'       => 'site_item_invitations#invitations',        as: :invitations_site
  post    '/sites/:id/send_invitations'               => 'site_item_invitations#send_invitations',   as: :send_invitations_site
  delete  '/sites/:id/delete_invitations'             => 'site_item_invitations#delete_invitations', as: :delete_invitations_site
  get     '/sites/:id/get_invitations'                => 'site_item_invitations#get_invitations',    as: :get_invitations_site
  post    '/sites/:id/save_invitation/:site_item_id'  => 'site_item_invitations#save_invitation',    as: :site_save_invitation
  post    '/invitation/:id/pause'                     => 'site_item_invitations#pause_invitation',   as: :site_pause_invitation
  delete  '/invitation/:id/delete'                    => 'site_item_invitations#delete_invitation',  as: :site_delete_invitation
  put     '/invitation/:id/update'                    => 'site_item_invitations#update_invitation',  as: :site_update_invitation
  
  # Site Item Documents
  get     '/sites/:id/documents/:site_item_id'        => 'site_item_documents#documents',            as: :site_documents
  post    '/sites/:id/document/:site_item_id'         => 'site_item_documents#create_document',      as: :create_document
  get     '/document/:id/current'                     => 'site_item_documents#current_document',     as: :current_document
  get     '/document/:id/approve'                     => 'site_item_documents#approve_document',     as: :approve_document
  get     '/document/:id/pending'                     => 'site_item_documents#pending_document',     as: :pending_document
  get     '/document/:id/archive'                     => 'site_item_documents#archive_document',     as: :archive_document
  get     '/document/:id/delete'                      => 'site_item_documents#delete_document',      as: :delete_document
  get     '/document/:id/reject'                      => 'site_item_documents#reject_document',      as: :reject_document
  post    '/document/:id/due_soon'                    => 'site_item_documents#due_soon',             as: :due_soon_document
  get     '/document/:id/feedback'                    => 'site_item_documents#feedback',             as: :feedback_document
  post    '/document/:id/feedback'                    => 'site_item_documents#save_feedback',        as: :save_feedback_document
  
  resources :item_templates, except: [:show, :new, :edit]
  get     '/item_templates/:id/destroy'               => 'item_templates#destroy_show',              as: :item_template_destroy_show
  
  resources :reports, except: [:show, :edit, :update]
  
  get     '/about_us'                                 => 'home#about_us',                            as: :about_us
  get     '/contact_us'                               => 'home#contact_us',                          as: :contact_us
  post    '/contact_us'                               => 'home#save_contact_us',                     as: :save_contact_us
  get     '/pricing'                                  => 'home#pricing',                             as: :pricing
  get     '/faq'                                      => 'home#faq',                                 as: :faq
  get     '/why_us'                                   => 'home#why_us',                              as: :why_us
  get     '/terms_and_conditions'                     => 'home#terms_and_conditions',                as: :terms_and_conditions
  get     '/privacy_policy'                           => 'home#privacy_policy',                      as: :privacy_policy
  get     '/news'                                     => 'home#news',                                as: :news
  get     '/news/:id'                                 => 'home#news_show',                           as: :news_show
  post    '/news/:id/comment'                         => 'home#create_comment',                      as: :create_comment
  get     '/comment/:id/reply'                        => 'home#new_reply',                           as: :new_reply
  post    '/comment/:id/reply'                        => 'home#create_reply',                        as: :create_reply
  # services
  get     '/services'                                 => 'home#services',                            as: :services
  get     '/service/:id'                              => 'home#service_show',                        as: :service_show
  # teams
  get     '/teams'                                    => 'home#teams',                               as: :teams
  get     '/team/:id'                                 => 'home#team_show',                           as: :team_show
  # Send inquiry
  get     '/inquiry'                                  => 'home#inquiry',                             as: :inquiry                 
  post    '/create_inquiry'                           => 'home#create_inquiry',                      as: :create_inquiry                 
  # features
  get     '/features'                                 => 'home#features',                            as: :features
  get     '/feature/:id'                              => 'home#feature_show',                        as: :feature_show
  # Create Newsletter
  post    '/create_newsletter'                        => 'home#create_newsletter',                   as: :create_newsletter
  
  # mount Ckeditor::Engine => '/ckeditor'
  get     '/404'                                      => 'errors#not_found'
  get     '/500'                                      => 'errors#internal_server_error'
  
  devise_scope :user do
    authenticated :user do
      root to: 'sites#index'
    end
    unauthenticated :user do
      root to: 'home#index', as: :unauthenticated_root
    end
  end
  get '*path' => redirect('/')
end
