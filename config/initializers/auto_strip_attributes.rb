AutoStripAttributes::Config.setup do
  set_filter strip_html: false do |value|
    ActionController::Base.helpers.strip_tags value
  end
  
  filters_order.delete(:strip_html)
  filters_order.insert(0, :strip_html)
  filters_enabled[:squish] = true
end