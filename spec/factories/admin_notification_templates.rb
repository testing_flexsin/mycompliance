FactoryGirl.define do
  factory :admin_notification_template do
    user_id 1
invitee_template "MyText"
email_due_soon_template "MyText"
email_overdue_template "MyText"
email_due_soon_invitee_template "MyText"
email_overdue_invitee_template "MyText"
email_report_recipients_template "MyText"
document_feedback_approved_template "MyText"
document_feedback_pending_template "MyText"
document_feedback_rejected_template "MyText"
  end

end
