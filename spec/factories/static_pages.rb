FactoryGirl.define do
  factory :static_page do
    logo "MyString"
header_logged_in "MyText"
header_not_logged_in "MyText"
footer "MyText"
features "MyText"
about_us "MyText"
terms_and_conditions "MyText"
privacy_policy "MyText"
faq "MyText"
contact_us "MyText"
services "MyText"
pricing "MyText"
news "MyText"
team "MyText"
testimonials "MyText"
  end

end
